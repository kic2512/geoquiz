from geoquiz import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from social.apps.django_app.middleware import SocialAuthExceptionMiddleware
from social.exceptions import AuthCanceled, AuthStateMissing, AuthMissingParameter


class Proxy(object):
    def process_request(self, request):
        request.META['HTTP_X_FORWARDED_HOST'] = settings.HOSTNAME


class SocialAuthMiddleware(SocialAuthExceptionMiddleware):
    def process_exception(self, request, exception):
        if isinstance(exception, AuthCanceled):
            redirect_url = reverse('core:index')
            return HttpResponseRedirect(redirect_url)
        elif isinstance(exception, AuthStateMissing) or isinstance(exception, AuthMissingParameter):
            redirect_url = reverse('core:index')
            return HttpResponseRedirect(redirect_url)
        else:
            pass
            # raise exception