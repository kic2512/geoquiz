$('#js-reg').on('click', function(){

    var formData = new FormData($('#js-signup-form')[0]);
    var url = $('#js-signup-form').attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response){

            if('data' in response){
                if('message' in response['data']){
                    var block = $('#success-reg-block');

                    block.fadeOut(0);
                    $('#help-reg-block').fadeOut(0);

                    block.html(response['data']['message']);
                    block.fadeIn(1500, function(){

                        window.location.href = '/';
                    });
                }
            }

            if('error' in response){
                $('#success-reg-block').fadeOut(0);
                $('#help-reg-block').fadeIn(500);
                $('#help-reg-block').html(response['error']);
            }
        },

        error: function(response){
                alert('Error: see console log;');
                console.log(response.responseText);
        }
    });
});