$('#js-login').on('click', function(){

    var formData = new FormData($('#js-login-form')[0]);
    var url = $('#js-login-form').attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response){
            if('data' in response){
                if('message' in response['data']){
                    var block = $('#success-block');

                    $('#help-block').fadeOut(0);
                    block.fadeOut(0);

                    block.html(response['data']['message']);
                    block.fadeIn(500, function(){
                        block.fadeOut(500, function(){
                            if('redirect_url' in response['data']){
                                window.location.href = response['data']['redirect_url'];
                            }
                        });
                    });
                }
            }

            if('error' in response){
                $('#success-block').fadeOut();
                $('#help-block').fadeIn(500);
                $('#help-block').html(response['error']);
            }
        },

        error: function(response){
                alert('Error: see console log;');
                console.log(response.responseText);
        }
    });
});