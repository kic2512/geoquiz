$(document).ready(function () {
    var values = [],
        kyes = Object.keys(localStorage),
        i = kyes.length;

    while (i--) {

        var queId = kyes[i];
        var choiceId = localStorage.getItem(queId);
        var params = {
                choice: choiceId
        };

        $.post("/answer-question/", params)
            .done(function (response) {
                if (response.status === 'OK') {
                    console.log("Вопрос сохранен:");
                    localStorage.removeItem(queId);
                }
                else {
                    console.log(response.error);
                }
            })
            .fail(function (jqXHR, textStatus) {
                console.log('Проверьте сетевое подключение');
            });
    }    
});


$('#js-send-form').on('click', function () {
    var url = $('#js-user-form').attr('action');

    var formData = new FormData($('#js-user-form')[0]);

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            if ('status' in response) {
                if (response['status'] == 'OK') {
                    for (var field in response['data']) {
                        if (response['data'].hasOwnProperty(field)) {
                            $('[name=' + field + ']').val(response['data'][field]);
                            console.log($('[name=' + field + ']').val());
                        }
                    }
                }
            }

            if ('error' in response) {
                alert(response['error']);
            }
        },
        error: function (response) {
            alert('Error: see console log;');
            console.log(response.responseText);
        }
    });
});