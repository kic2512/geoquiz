import requests
from datetime import datetime

from core.models import Country, City
from django.db.models import Q

__author__ = 'kic'


def get_country(name):
    if not name:
        return None
    if name == 'Russia':
        name = 'Russian Federation'

    if name == 'Россия':
        name = 'Russian Federation'

    country = Country.objects.filter(Q(original_name=name) | Q(international_name=name) | Q(
            cyrillic_name=name)).first()
    return country


def get_city_by_country(name, country):
    if not country or not name:
        return None
    city = City.objects.filter(country=country).filter(
            Q(original_name=name) | Q(international_name=name) | Q(
                    cyrillic_name=name)).first()

    return city
