from __future__ import absolute_import

import hashlib
from django.db.models.loading import get_model
from geoquiz.celery import app
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut, GeocoderServiceError
from django.apps import apps
from django.contrib.gis.geos import Point
from requests import request, HTTPError
from django.core.files.base import ContentFile

__author__ = 'kic'

MAX_RERUN_COUNT = 3
SUCCESS = 0
ERROR = -1


@app.task
def get_lat_lng(user_id, country_name=None, city_name=None, rerun=None):
    if rerun and rerun > MAX_RERUN_COUNT:
        print('MAX RERUN COUNT FOR USER WITH ID {0}'.format(user_id))
        return 0

    model_user = apps.get_model(app_label='core', model_name='User')
    user = model_user.objects.filter(id=user_id).first()

    if not user:
        return None

    geolocator = Nominatim()

    if country_name:
        location = None
        try:
            location = geolocator.geocode(country_name)

        except GeocoderTimedOut:
            if rerun is None:
                rerun = 1
            get_lat_lng.apply_async((user_id, country_name, city_name, rerun + 1), countdown=5)

        except GeocoderServiceError:
            if rerun is None:
                rerun = 1
            get_lat_lng.apply_async((user_id, country_name, city_name, rerun + 1), countdown=10)

        if location:
            user.country_coordinates = Point(location.latitude, location.longitude)
            user.country = location.raw['display_name']

    if city_name:
        location = None
        try:
            location = geolocator.geocode(city_name)

        except GeocoderTimedOut:
            if rerun is None:
                rerun = 1
            get_lat_lng.apply_async((user_id, country_name, city_name, rerun + 1), countdown=5)

        except GeocoderServiceError:
            if rerun is None:
                rerun = 1
            get_lat_lng.apply_async((user_id, country_name, city_name, rerun + 1), countdown=10)

        if location:
            user.city_coordinates = Point(location.latitude, location.longitude)
            city_name = get_short_name(location.raw['display_name'])
            user.city = city_name

    user.save()

    return SUCCESS


def get_short_name(full_name):
    name = full_name.split(',')
    return name[0]


@app.task
def upload_avatar(user_id, avatar_link):
    user_model = get_model('core', 'User')
    user = user_model.objects.filter(id=user_id).first()

    VALID_CONTENT_TYPES = {
        'image/gif': 'gif',
        'image/jpeg': 'jpg',
        'image/jpg': 'jpg',
        'image/pjpeg': 'jpg',
        'image/png': 'png',
        'image/svg+xml': 'svg',
    }

    if user:
        try:
            data = request('GET', avatar_link)
            data.raise_for_status()
            content_type = data.headers['Content-Type']
            if content_type in VALID_CONTENT_TYPES:
                extension = VALID_CONTENT_TYPES[content_type]
                prefix = hashlib.new('md5', avatar_link.encode('utf-8')).hexdigest()[:10]
                user.avatar.save('{0}.{1}'.format(prefix, extension), ContentFile(data.content), save=False)
                user.save()
                return SUCCESS
        except HTTPError:
            print('Http error while save avatar')

    return ERROR


@app.task
def update_lat_lng(country_id=None, city_id=None, rerun=None):
    if rerun and rerun > MAX_RERUN_COUNT:
        print('MAX RERUN COUNT')
        return 0

    geolocator = Nominatim()

    if country_id:
        country_model = apps.get_model(app_label='core', model_name='Country')
        country = country_model.objects.filter(id=country_id).first()

        location = None
        try:
            location = geolocator.geocode(country.international_name)

        except GeocoderTimedOut:
            if rerun is None:
                rerun = 1
            get_lat_lng.apply_async((country_id, country_id, rerun + 1), countdown=5)

        except GeocoderServiceError:
            if rerun is None:
                rerun = 1
            get_lat_lng.apply_async((country_id, city_id, rerun + 1), countdown=10)

        if location:
            country.coordinates = Point(location.latitude, location.longitude)

    if city_id:

        city_model = apps.get_model(app_label='core', model_name='City')
        city = city_model.objects.filter(id=city_id).first()

        location = None
        try:
            location = geolocator.geocode(city.international_name)

        except GeocoderTimedOut:
            if rerun is None:
                rerun = 1
            get_lat_lng.apply_async((country_id, city_id, rerun + 1), countdown=5)

        except GeocoderServiceError:
            if rerun is None:
                rerun = 1
            get_lat_lng.apply_async((country_id, city_id, rerun + 1), countdown=10)

        if location:
            city.coordinates = Point(location.latitude, location.longitude)
            city.save()

    return SUCCESS
