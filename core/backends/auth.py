from core.models import User

__author__ = 'kic'


class TokenBackend(object):
    def authenticate(self, token):
        user = User.objects.filter(token=token).first()
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None