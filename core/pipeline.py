from datetime import datetime
from core.tasks import upload_avatar
import urllib
import json
import requests
from core.forms import UserProfileForm
from core.models import User, City
from core.utils import get_country, get_city_by_country

__author__ = 'kic'


def extra_user_details(strategy, details, user=None, *args, **kwargs):
    if user:
        if 'backend' in kwargs:
            if kwargs['backend'].name == 'facebook':
                handle_facebook_response(strategy, user, **kwargs)
            elif kwargs['backend'].name == 'vk-oauth2':
                handle_vk_response(strategy, user, **kwargs)


def handle_facebook_response(strategy, user, **kwargs):
    facebook_genders = {
        'male': 'M',
        'female': 'F',
    }

    if 'response' in kwargs:
        response = kwargs['response']

        if not user.email and 'email' in response:
            user.email = response['email']

        if 'gender' in response:
            user.gender = facebook_genders.get(response.get('gender'), 'N')

        if 'birthday' in response:
            try:
                user.date_of_birth = datetime.strptime(response['birthday'], '%m/%d/%Y').date()
            except Exception:
                pass  # API was changed date format

        if not user.city and 'hometown' in response:
            try:
                location = response['hometown'].get('name', '')
                location = location.split(',')
                user.country = get_country(location[1].strip())
                user.city = get_city_by_country(location[0].strip(), user.country)
            except Exception:
                pass  # API was changed hometown data format

        if 'picture' in response:
            if 'data' in response['picture']:
                if 'url' in response['picture']['data']:
                    avatar_link = response['picture']['data']['url']
                    upload_avatar.apply_async((user.id, avatar_link))

        strategy.storage.user.changed(user)


def handle_vk_response(strategy, user, **kwargs):
    if 'response' in kwargs:
        response = kwargs['response']

        vk_genders = {
            0: 'N',
            1: 'F',
            2: 'M',
        }

        if user.gender == 'N' and 'sex' in response:
            user.gender = vk_genders.get(response.get('sex'), 'N')

        if not user.date_of_birth:
            if 'bdate' in response:
                try:
                    user.date_of_birth = datetime.strptime(response['bdate'], '%d.%m.%Y').date()
                except Exception:
                    pass

        if not user.country and 'country' in response:
            try:
                country_name = response['country'].get('title', '')
                user.country = get_country(country_name)
            except Exception:
                pass

        if not user.city and 'city' in response:
            try:
                city_name = response['city'].get('title', '')
                user.city = get_city_by_country(city_name, user.country)
                if not user.city and user.country:
                    c = City(international_name=city_name, cyrillic_name=city_name, country=user.country)
                    c.save()
            except Exception:
                pass

        if 'photo' in response:
            avatar_link = response['photo']
            upload_avatar.apply_async((user.id, avatar_link))

        strategy.storage.user.changed(user)
