from django.conf.urls import url

from core.views import UserProfile, EditUserProfile, LoginView, RegistrationView, ActivationView, UploadAvatar, \
    LogoutView, DevView, IdentifyView


urlpatterns = [
    url(r'^$', DevView.as_view(), name="index"),

    url(r'^profile/', UserProfile.as_view(), name='profile'),
    url(r'^edit_profile/', EditUserProfile.as_view(), name='edit_profile'),
    url(r'^change_avatar/$', UploadAvatar),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^reg/$', RegistrationView.as_view(), name='reg'),
    url(r'^activate/$', ActivationView.as_view(), name='activate'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^identify/$', IdentifyView.as_view(), name='identify')
]
