from core.tasks import get_lat_lng, update_lat_lng
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.db import models
from django.templatetags.static import static
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import hashlib
from django.contrib.gis.db import models as gis_models


class Country(models.Model):
    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    original_name = models.CharField(max_length=128)
    international_name = models.CharField(max_length=128)
    cyrillic_name = models.CharField(max_length=128, default='')

    coordinates = gis_models.PointField(_('Country coordinates'), null=True, blank=True)
    external_id = models.IntegerField(null=True)

    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.international_name


class City(models.Model):
    class Meta:
        verbose_name = _('City')
        verbose_name_plural = _('Cities')

    original_name = models.CharField(max_length=128)
    international_name = models.CharField(max_length=128)
    cyrillic_name = models.CharField(max_length=128, default='')

    coordinates = gis_models.PointField(_('City coordinates'), null=True, blank=True)

    country = models.ForeignKey(Country)

    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.international_name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super().save(force_insert=False, force_update=False, using=None,
                     update_fields=None)

        if not self.coordinates:
            update_lat_lng.apply_async((None, self.id), )


class UserManager(BaseUserManager):
    def _create_user(self, username, password, is_staff, is_superuser, is_activated, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        user = self.model(username=username,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        if 'email' in extra_fields:
            email = self.normalize_email(extra_fields['email'])
            if email:
                user.email = email
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        return self._create_user(username, password, False, False, False, **extra_fields)

    def create_superuser(self, username, password=None, **extra_fields):
        return self._create_user(username, password, True, True, True,  **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('User')

    GENDER = (
        ('M', _('Male')),
        ('F', _('Female')),
        ('N', _('None')),
    )

    email = models.EmailField(_('Email'), max_length=128)
    username = models.CharField(_('Username'), max_length=100, unique=True)
    first_name = models.CharField(_('First name'), max_length=100, blank=True)
    last_name = models.CharField(_('Last name'), max_length=100, blank=True)
    token = models.CharField(max_length=128, blank=True, null=True)

    gender = models.CharField('Gender', max_length=1, choices=GENDER, default='N', blank=True)

    date_of_birth = models.DateField(_('Birthday'), blank=True, null=True)

    country = models.ForeignKey(Country, null=True, blank=True, verbose_name=_('Country'))

    city = models.ForeignKey(City, null=True, blank=True, verbose_name=_('City'))

    date_joined = models.DateTimeField(_('Date joined'), default=timezone.now)

    is_staff = models.BooleanField(_('Is staff'), default=False)
    is_activated = models.BooleanField(_('Is activated'), default=False)
    is_active = models.BooleanField(_('Is active'), default=True)

    avatar = models.ImageField(upload_to='avatars/', default=static('core/img/default-avatar.png'))

    objects = UserManager()
    USERNAME_FIELD = 'username'

    def __str__(self):
        return self.email

    def as_dict(self):
        country = None
        city = None

        if self.country:
            country = self.country.cyrillic_name,

        if self.city:
            city = self.city.cyrillic_name,

        return dict(
                email=self.email,
                first_name=self.first_name,
                last_name=self.last_name,
                country=country,
                city=city,
                gender=self.gender,
                username=self.username,
                date_of_birth=self.date_of_birth,
                avatar=self.avatar.url
        )

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        if self.first_name:
            if self.last_name:
                return self.first_name + ' ' + self.last_name
            else:
                return self.first_name
        else:
            return self.username

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = get_token('{0}{1}'.format(self.id, self.username))

        super(User, self).save(*args, **kwargs)


def get_token(base):
    return hashlib.md5(base.encode('utf-8')).hexdigest()
