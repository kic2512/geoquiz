import json as json_lib

from core.forms import UserProfileForm
from core.models import User, City, Country
from core.utils import get_country, get_city_by_country
from django import forms
from django.contrib.auth.views import logout
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.core.validators import validate_email
from django.db import IntegrityError
from django.db.models import Q
from geoquiz import settings
from django.http import Http404
from django.views.generic import RedirectView
from django.shortcuts import redirect
from quiz.models import Question, Choice, Answer
from django.contrib import auth
from django.contrib.auth import authenticate, login
from django.http import HttpResponseBadRequest
from django.views.generic import TemplateView
from utils.json_views import JsonView
from core.forms import AvatarUploadForm
from utils.access import LoginRequiredMixin
from django.core.exceptions import ValidationError
from datetime import datetime

REDIRECT_MAP = {
    0: 'profile'
}

REDIRECT_MAP_REVERSE = {
    'profile': 0
}


class DevView(TemplateView):
    http_method_names = ['get']
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        self.redirect_id = None
        if 'redirect_id' in request.session:
            self.redirect_id = request.session['redirect_id']
            del request.session['redirect_id']
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.redirect_id is not None:
            front_end_navigate = REDIRECT_MAP[self.redirect_id]
            context['navigate'] = front_end_navigate

        context['available_countries'] = str(json_lib.dumps(
                    [country.cyrillic_name for country in Country.objects.all().order_by('cyrillic_name')]))
        return context


class UserProfile(LoginRequiredMixin, TemplateView):
    http_method_names = ['get']
    template_name = 'core/user.html'

    def get_context_data(self, **kwargs):
        context = super(UserProfile, self).get_context_data()
        user = self.request.user
        if user:
            context['user_form'] = UserProfileForm(user.as_dict())
            context['user_avatar'] = AvatarUploadForm(user.as_dict())
            context['avatar'] = user.avatar.url

            user_questions = Question.objects.filter(author_id=user.id).order_by('-pub_date')[:5]
            context['user_questions'] = user_questions

            user_choice = Answer.objects.filter(user_id=user.id).values_list('choice_id', flat=True)
            user_answer = Choice.objects.filter(id__in=user_choice).values_list('question_id', flat=True)
            user_passed_questions = Question.objects.filter(id__in=user_answer).order_by('-pub_date')[:5]
            context['user_passed_questions'] = user_passed_questions
        return context


# class TestQuizView(TemplateView):
#     http_method_names = ['get']
#     template_name = 'core/test_forms.html'


class EditUserProfile(JsonView):
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):

        try:
            data = json_lib.loads(request.body.decode("utf-8"))
        except Exception as e:
            return self.json_error('Неверный запрос')

        self.user_dict = data

        is_email_valid = True
        try:
            validate_email(self.user_dict.get('email', ''))
        except ValidationError:
            is_email_valid = False

        if 'email' not in self.user_dict or not is_email_valid:
            return self.json_error('Введите корректный email')

        return super().post(request, *args, **kwargs)

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)

        logined_user = auth.get_user(self.request)

        user = User.objects.filter(username=logined_user.username).first()
        if not user:
            json = self.get_error_data()
            json['error'] = 'Пользователь не найден'
            return json

        user.email = self.user_dict['email']

        if 'first_name' in self.user_dict:
            user.first_name = self.user_dict['first_name']

        if 'last_name' in self.user_dict:
            user.last_name = self.user_dict['last_name']

        if 'gender' in self.user_dict:
            user.gender = self.user_dict['gender']

        if 'date_of_birth' in self.user_dict:
            if self.user_dict['date_of_birth']:
                try:
                    user.date_of_birth = datetime.strptime(self.user_dict['date_of_birth'], '%Y-%m-%d').date()
                except ValueError:  # invalid date format
                    json = self.get_error_data()
                    json['error'] = 'Неверный формат даты'
                    return json

        if 'country' in self.user_dict:
            new_country_name = self.user_dict['country']
            user.country = get_country(new_country_name)

        if 'city' in self.user_dict:
            new_city_name = self.user_dict['city']
            user.city = get_city_by_country(new_city_name, user.country)

        try:
            user.save()
            json['status'] = 'OK'
            json['data'] = user.as_dict()

        except IntegrityError:
            json = self.get_error_data()
            json['error'] = 'Пользователь с таким email уже существует'
            return json

        except Exception as e:
            json = self.get_error_data()
            json['error'] = 'Сервер временно не доступен попробуйте повторить попытку позже'
            return json

        return json


def UploadAvatar(request):
    if request.method == 'POST':
        form = AvatarUploadForm(request.POST, request.FILES, )

        if form.is_valid():
            task = form.save(commit=False)

            logined_user = auth.get_user(request)
            user = User.objects.filter(username=logined_user.username).first()
            if not user:
                return HttpResponseBadRequest()
            user.avatar = task.avatar
            user.save()

            return redirect('/profile/')
    return HttpResponseBadRequest()


class RegistrationView(JsonView):
    http_method_names = ['post']

    SUCCESS = 0
    INVALID_EMAIL = 1
    YET_EXISTS = 2

    STATE_MSG = {
        SUCCESS: 'На вашу электронную почту отправлны инструкции по активации',
        INVALID_EMAIL: 'Введенный email некорректен',
        YET_EXISTS: 'Пользователь с такой электронной почтой уже зарегистрирован'
    }

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        password = request.POST.get('password')

        if not email or not password:
            return HttpResponseBadRequest()

        self.state = self.do_register(email, password)
        return super().post(request, *args, **kwargs)

    def send_activation_email(self, user):
        subject = 'GeoPoll регистрация'
        link = 'http://{0}{1}?token={2}'.format(settings.HOSTNAME, reverse('core:activate'), user.token)
        html_content = 'Здравствуйте!\nДля активации на портале geopoll.info перейдите по ссылке:\n{0}\n'.format(link)

        send_mail(subject, html_content, settings.SENDER_EMAIL, [user.email],
                  html_message=html_content)

    def do_register(self, email, password):
        try:
            validate_email(email)
        except forms.ValidationError:
            return self.INVALID_EMAIL

        username = email
        user = User.objects.filter(Q(username=username) | Q(email=email))
        if not user:
            user = User(username=username, email=email, is_activated=False)
            user.set_password(password)
            user.save()

            self.send_activation_email(user)
            return self.SUCCESS
        else:
            return self.YET_EXISTS

    def get_json_data(self, **kwargs):
        if self.state == self.SUCCESS:
            json = super().get_json_data(**kwargs)
            json['data'] = {
                'message': self.STATE_MSG[self.state]
            }
        else:
            json = super().get_error_data(**kwargs)
            json['error'] = self.STATE_MSG[self.state]
        return json


class LoginView(JsonView):
    http_method_names = ['post']

    SUCCESS = 0
    NOT_ACTIVATED = 1
    NOT_EXISTS = 2

    STATE_MSG = {
        SUCCESS: 'Вы успешно вошли на сайт',
        NOT_ACTIVATED: 'Ваш аккаунт не был активирован',
        NOT_EXISTS: 'Пользователь с таким email и паролем не зарегистрирован'
    }

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        password = request.POST.get('password')

        if not email or not password:
            return HttpResponseBadRequest()

        self.state = self.do_login(email, password)
        return super().post(request, *args, **kwargs)

    def do_login(self, email, password):
        user = authenticate(username=email, password=password)
        if user is not None:
            if user.is_active and user.is_activated:
                login(self.request, user)
                self.user = user
                return self.SUCCESS
            else:
                return self.NOT_ACTIVATED
        else:
            return self.NOT_EXISTS

    def get_json_data(self, **kwargs):
        if self.state == self.SUCCESS:
            json = super(LoginView, self).get_json_data()
            json['data'] = {
                'user': self.user.as_dict(),
                'message': self.STATE_MSG[self.state]
            }
        else:
            json = super(LoginView, self).get_error_data()
            json['error'] = self.STATE_MSG[self.state]
        return json


class ActivationView(RedirectView):
    # User get this view from email link

    http_method_names = ['get']
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        token = self.request.GET.get('token')

        if not token:
            raise Http404

        user = User.objects.filter(token=token).first()

        if not user:
            raise Http404

        user.is_activated = True
        user.save()

        if user.is_active:
            user = authenticate(token=token)
            login(self.request, user)
            root = reverse('core:index')
            url_hash = 'profile'
            return '{0}#{1}'.format(root, url_hash)

        else:
            raise Http404


class LogoutView(JsonView):
    http_method_names = ['get']

    def get(self, *args, **kwargs):
        logout(self.request)
        return super(LogoutView, self).get(*args, **kwargs)

    def get_json_data(self, **kwargs):
        json = super(LogoutView, self).get_json_data(**kwargs)
        json['data'].update({
            'message': 'Приходите снова!'
        })
        return json


class IdentifyView(LoginRequiredMixin, JsonView):
    http_method_names = ['get']

    def get_json_data(self, **kwargs):
        json = super(IdentifyView, self).get_json_data(**kwargs)
        user_data = self.request.user.as_dict()
        json['data'].update(user_data)
        return json
