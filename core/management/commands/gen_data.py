import random
from core.management.commands.constants import questions_pull, first_name_pull, last_names_pull, country_pull, ru_cities
from core.management.commands.utils import update_progress
from core.models import User, Country, City
from django.db import IntegrityError
from optparse import make_option
from django.core.management import BaseCommand
from quiz.models import Question, Choice, Answer
from django.utils import timezone

__author__ = 'kic'

DEFAULT_USERS_COUNT = 20000


class Command(BaseCommand):

    def handle(self, *args, **options):
        main()


def main():
    users_cnt = DEFAULT_USERS_COUNT
    users_id_list = create_users(users_cnt)
    create_questions(users_id_list)


def create_users(count):
    if count < 0:
        exit(1)
    users_id_list = []
    i = 0
    duplicate = 0
    print('Start users creating')

    countries = Country.objects.filter(external_id__in=[2,14,15,30,35,47,55,63,66,69,74,76,77,82,87,101,102,107,110,121,133,152,169,174,178,185,191,206,213,216,223
,224,225,])

    countries_list = [country for country in countries]

    while i < count:
        first_name = random.choice(first_name_pull)
        last_name = random.choice(last_names_pull)
        email = '{0}-{1}@{2}.com'.format(first_name, random.randint(0, 9999), last_name)
        username = email

        country = random.choice(countries_list)
        city = City.objects.filter(country=country).order_by('?').first()

        start_date = timezone.now().today().replace(year=1970).toordinal()
        end_date = timezone.now().today().replace(year=1999).toordinal()
        random_date = timezone.now().fromordinal(random.randint(start_date, end_date))

        gender = random.choice(['M', 'F'])

        user = User(first_name=first_name, last_name=last_name, email=email, username=username, country=country,
                    date_of_birth=random_date, gender=gender, city=city, is_activated=True)
        try:
            user.set_password('0')
            user.save()
            i += 1
            users_id_list.append(user.id)
            update_progress(int(i * 100.0 / count))
        except IntegrityError:
            duplicate += 1

    print('Users successfully created')

    if duplicate > 0:
        print('Duplicates users {0}'.format(duplicate))

    return users_id_list


def create_questions(users_id_list):
    print('Start questions creating')

    questions_duplicate = 0
    choices_duplicate = 0
    answers_duplicate = 0

    i = 0
    count = len(questions_pull)

    users_offset_max = 15000
    users_limit = 5000

    update_progress(int(i * 100.0 / count))

    for quest_txt in questions_pull:

        quest = Question(author_id=random.choice(users_id_list), title=quest_txt)

        try:
            quest.save()
        except IntegrityError:
            questions_duplicate += 1

        choice_id_list = []

        for choice_txt in questions_pull[quest_txt]:
            choice = Choice(question=quest, text=choice_txt)
            try:
                choice.save()
                choice_id_list.append(choice.id)
            except IntegrityError:
                choices_duplicate += 1

        current_offset = random.randint(0, users_offset_max)
        end = current_offset + users_limit
        users_answer_for_question = users_id_list[current_offset:end]

        for usr in users_answer_for_question:
            start_date = timezone.now().today().replace(year=2010).toordinal()
            end_date = timezone.now().today().replace(year=2016).toordinal()
            random_date = timezone.now().fromordinal(random.randint(start_date, end_date))

            ans = Answer(user_id=usr, choice_id=random.choice(choice_id_list), create_date=random_date)
            try:
                ans.save()
            except IntegrityError:
                answers_duplicate += 1

        i += 1
        update_progress(int(i * 100.0 / count))

    if questions_duplicate > 0:
        print('Duplicates questions {0}'.format(questions_duplicate))

    if choices_duplicate > 0:
        print('Duplicates choices {0}'.format(choices_duplicate))

    if answers_duplicate > 0:
        print('Duplicates answers {0}'.format(answers_duplicate))

    print('Questions successfully created')
