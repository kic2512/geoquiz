from random import randint
from optparse import make_option

from core.management.commands.utils import update_progress
from core.models import User, Country, City
from django.core.management import BaseCommand
from django.contrib.gis.geos import Point
import sys
from django.db.models import Count

__author__ = 'kic'


class Command(BaseCommand):
    def handle(self, *args, **options):

        max_cities_from_country = 3
        min_cities_from_country = 1

        country_cities_counts = {}

        records = 1244157
        i = 0

        available_countries_external_id = [14, 55, 66, 69, 74, 76, 77, 82, 87, 101, 102, 107, 133, 152, 169, 174, 178,
                                           185, 191, 216]

        f = open('cities.csv', 'r')
        for line in f:
            i += 1
            values = line.split(';')
            if len(values) >= 4:
                name = values[0]
                country_external_id = values[1]
                lat = float(values[2])
                lng = float(values[3])

                if int(country_external_id) not in available_countries_external_id:
                    continue

                if country_external_id not in country_cities_counts:
                    country_cities_counts[country_external_id] = {
                        'count': 0,
                        'max': randint(min_cities_from_country, max_cities_from_country),
                        'object': Country.objects.filter(external_id=country_external_id).first()
                    }

                current = country_cities_counts[country_external_id]
                if current['count'] < current['max']:
                    if current['object']:
                        country = current['object']
                        city = City(international_name=name, coordinates=Point(lat, lng), country=country)
                        city.save()
                        current['count'] += 1

            update_progress(int(i * 100.0 / records))
        f.close()
