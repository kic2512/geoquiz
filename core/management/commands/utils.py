import sys


def update_progress(percent):
    line_width = 100
    progress = int(percent * 100.0 / line_width)
    lost = line_width - progress
    sys.stdout.write('\r[{0}{1}] {2}%'.format('#' * progress, ' ' * lost, percent))
    sys.stdout.flush()

    if percent == 100:
        sys.stdout.write('\r\n')
