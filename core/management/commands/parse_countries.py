from optparse import make_option

from core.management.commands.utils import update_progress
from core.models import User, Country
from django.core.management import BaseCommand
from django.contrib.gis.geos import Point
import sys
from django.db.models import Count

__author__ = 'kic'


class Command(BaseCommand):
    def handle(self, *args, **options):
        f = open('countries_with_cyrillic.csv', 'r')
        records = 239.0
        i = 0
        for line in f:
            i += 1
            values = line.split(';')
            if len(values) > 4:
                external_id = values[0]
                name = values[1]
                cyr_name = values[2]
                lat = float(values[3])
                lng = float(values[4])
                country = Country(international_name=name, cyrillic_name=cyr_name, coordinates=Point(lat, lng),
                                  external_id=external_id)
                country.save()

            update_progress(int(i * 100.0 / records))
        f.close()
