from collections import OrderedDict
from core.models import User
from quiz.models import Question
from django import forms
from django.forms import EmailInput, PasswordInput, TextInput, RadioSelect, ChoiceField
from django.forms.widgets import RadioFieldRenderer
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.templatetags.static import static

__author__ = 'kic'


class GenderRenderer(RadioFieldRenderer):
    def render(self):
        genders = OrderedDict()

        genders['M'] = ['Male', static('core/img/male.png'), 'M']
        genders['F'] = ['Female', static('core/img/female.png'), 'F']

        value = None
        if self.value:
            value = self.value

        for w in self:
            genders[w.choice_value].append(w.tag())

        res = mark_safe('<div class="gender">' + ''.join(['''
                        <input type="radio" {3} class="button" id="{0}" name="gender" value={2}></input>
                        <label class="gen_field" for="{0}"><img width="64" height="64" src="{1}"></label>'''.format(
            genders[k][0], genders[k][1], genders[k][2], 'checked' if value == genders[k][2] else '') for k in
                                                          genders.keys()]) + '</div>')
        return res


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'first_name', 'gender', 'last_name', 'date_of_birth', 'country', 'city']
        widgets = {
            'email': EmailInput(attrs={'placeholder': 'Email', 'class': 'form-control'}),
            'first_name': TextInput(attrs={'placeholder': _('First name'), 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': _('Last name'), 'class': 'form-control'}),
            'date_of_birth': TextInput(attrs={'placeholder': _('Birth day'), 'class': 'form-control'}),
            'country': TextInput(attrs={'placeholder': _('Country'), 'class': 'form-control'}),
            'city': TextInput(attrs={'placeholder': _('City'), 'class': 'form-control'})
        }

    gender = ChoiceField(
        widget=RadioSelect(renderer=GenderRenderer),
    )


class UserRegistrationForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'password']
        widgets = {
            'email': EmailInput(attrs={'placeholder': 'Email'}),
            'password': PasswordInput(attrs={'placeholder': _('Password')}),
        }


class AvatarUploadForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('avatar', )