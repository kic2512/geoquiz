from quiz.models import Vote, Comment

__author__ = 'max'


def vote_types(request):
    return {
        'VOTE_QUESTION_TYPE': Vote.TYPE_QUESTION,
        'VOTE_CHOICE_TYPE': Vote.TYPE_CHOICE,
        'VOTE_COMMENT_TYPE': Vote.TYPE_COMMENT,
    }

def comment_types(request):
    return {
        'COMMENT_QUESTION_TYPE': Comment.TYPE_QUESTION
    }
