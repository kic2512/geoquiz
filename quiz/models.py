from django.db import models
from core.models import User
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist


class Question(models.Model):
    class Meta:
        verbose_name = _('Question')
        verbose_name_plural = _('Questions')

    ALL = 0
    POLITIC = 1
    MEDIA = 2
    EDUCATION = 3
    SPORT = 4
    JOB = 5
    HEALTH = 6
    LIFE = 7
    OTHER = 8

    TOPICS = (
        (POLITIC, _('Politic')),
        (MEDIA, _('Media')),
        (EDUCATION, _('Education')),
        (SPORT, _('Sport')),
        (JOB, _('Job')),
        (HEALTH, _('Health')),
        (LIFE, _('Life')),
        (OTHER, _('Other')),
        (ALL, _('All'))
    )

    author = models.ForeignKey(User)
    title = models.CharField(max_length=100)
    text = models.TextField(blank=True)

    topic = models.PositiveSmallIntegerField(_('Topic'), choices=TOPICS, default=OTHER)

    pub_date = models.DateTimeField(_('Date published'), default=timezone.now)

    rating = models.IntegerField(default=0)
    comments_cnt = models.IntegerField(default=0)
    original_language = models.CharField(max_length=20, default='ru')
    is_active = models.BooleanField(default=True)

    def get_comments_cnt(self):
        return Comment.objects.get_question_comments(self).count()

    def __str__(self):
        return self.title

    def as_dict(self):
        return dict(
                id=self.id,
                author_email=self.author.email,
                author_full_name=self.author.get_full_name(),
                title=self.title,
                text=self.text,
                pub_date=self.pub_date.strftime('%d.%m.%y %H:%M:%S'),
                rating=self.rating,
                original_language=self.original_language,
                comments_cnt=self.get_comments_cnt()
        )

    def get_absolute_url(self):
        return '/'


class Choice(models.Model):
    question = models.ForeignKey(Question)
    text = models.CharField(max_length=200)
    rating = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.text

    def as_dict(self):
        return dict(
                id=self.id,
                question=self.question.text,
                text=self.text,
                rating=self.rating
        )


class Tag(models.Model):
    question = models.ManyToManyField(Question)
    tag = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.tag

    def as_dict(self):
        return dict(
                id=self.id,
                tag=self.tag
        )


class AnswerManager(models.Manager):
    def question_is_answered(self, que, user):
        if not user.is_anonymous():
            answer = self.filter(choice__question=que, is_active=True, user=user).first()
            if answer:
                return answer.is_active
        return None


class Answer(models.Model):
    class Meta:
        unique_together = ('choice', 'user')

    choice = models.ForeignKey(Choice)
    user = models.ForeignKey(User)
    create_date = models.DateTimeField(_('Date answered'), default=timezone.now)
    is_active = models.BooleanField(default=True)

    objects = AnswerManager()

    def __str__(self):
        return str(self.choice.id) + ' : ' + str(self.user.id)

    def as_dict(self):
        return dict(
                id=self.id,
                question=self.question.text,
                create_date=self.create_date.strftime('%d.%m.%y %H:%M:%S'),
                user=self.user.email
        )


class VoteManager(models.Manager):
    @staticmethod
    def get_object(object_id, object_type):
        if object_type == Vote.TYPE_QUESTION:
            return Question.objects.get(id=object_id)
        elif object_type == Vote.TYPE_CHOICE:
            return Choice.objects.get(id=object_id)
        elif object_type == Vote.TYPE_COMMENT:
            return Comment.objects.get(id=object_id)
        else:
            raise ObjectDoesNotExist()

    def get_question_votes(self, question):
        return self.filter(object_id=question.id, object_type=Vote.TYPE_QUESTION, is_active=True).select_related('user')

    def get_choice_votes(self, choice):
        return self.filter(object_id=choice.id, object_type=Vote.TYPE_CHOICE, is_active=True).select_related('user')

    def get_comment_votes(self, comment):
        return self.filter(object_id=comment.id, object_type=Vote.TYPE_COMMENT, is_active=True).select_related('user')

    def question_is_voted(self, que, user):
        if not user.is_anonymous():
            vote = self.filter(object_id=que.id, object_type=Vote.TYPE_QUESTION, is_active=True, user=user).first()
            if vote:
                return vote.is_active
        return None


class Vote(models.Model):
    class Meta:
        """
        Vote == Like
        """
        unique_together = ('object_type', 'object_id', 'user')

    TYPE_QUESTION = 0
    TYPE_CHOICE = 1
    TYPE_COMMENT = 2

    TYPES = (
        (TYPE_QUESTION, _('Vote to question')),
        (TYPE_CHOICE, _('Vote to choice')),
        (TYPE_COMMENT, _('Vote to comment'))
    )

    object_type = models.PositiveIntegerField(choices=TYPES)
    object_id = models.PositiveIntegerField()
    user = models.ForeignKey(User)
    is_active = models.BooleanField(default=True)

    objects = VoteManager()

    def __str__(self):
        return _('Vote from ') + self.user

    def as_dict(self):
        return dict(
                id=self.id,
                object_id=self.object_id,
                object_type=self.object_type,
                value=self.is_active,
                user=self.user.email
        )


class CommentManager(models.Manager):
    @staticmethod
    def get_object(object_id, object_type):
        if object_type == Vote.TYPE_QUESTION:
            return Question.objects.get(id=object_id)
        else:
            raise ObjectDoesNotExist()

    def get_question_comments(self, question):
        return self.filter(object_id=question.id, object_type=Comment.TYPE_QUESTION, is_active=True) \
            .select_related('author')


class Comment(models.Model):
    TYPE_QUESTION = 0

    TYPES = (
        (TYPE_QUESTION, _('Comment to question')),
    )

    object_type = models.PositiveIntegerField(choices=TYPES)
    object_id = models.PositiveIntegerField()
    author = models.ForeignKey(User)
    text = models.TextField()
    is_active = models.BooleanField(default=True)
    pub_date = models.DateTimeField(default=timezone.now)

    objects = CommentManager()

    def __str__(self):
        return _('Comment from ') + self.author

    def as_dict(self):
        return dict(
                id=self.id,
                object_id=self.object_id,
                object_type=self.object_type,
                text=self.text,
                author_name=self.author.get_full_name(),
                pub_date=self.pub_date.strftime('%d.%m.%y %H:%M:%S'),
                author_email=self.author.email,
                author_avatar=self.author.avatar.url,
        )

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        super().save(force_insert=False, force_update=False, using=None,
                     update_fields=None)

        try:
            ENTITIES = [Question]

            type = self.object_type
            entity = ENTITIES[type]

            obj = entity.objects.get(id=self.object_id)
            obj.comments_cnt += 1
            obj.save()

        except (IndexError, ObjectDoesNotExist, AttributeError):
            pass
