from django.conf.urls import url
from django.views.generic import TemplateView
from quiz.views import QuestionListView, DetailQuestionView, VoteView, CreateQuestionView, AnswerQuestionView, \
    IndexView, \
    WonderQuestionView, EditQuestionView, DeleteQuestionView, CommentView, QuestionDistributionView, \
    UploadShareImg, AgeStatisticView, GenderStatisticView, PollDateStatisticView, CountriesStatisticView, \
    CitiesStatisticView, GeneralQuestionInfoView, CountryCityView, GetCitySuggests

urlpatterns = [
    url(r'^old/$', IndexView.as_view(), name='index'),
    url(r'^question-list/$', QuestionListView.as_view(), name='question_list'),
    url(r'^question-wonder/$', WonderQuestionView.as_view(), name='next_wonder'),
    url(r'^question-detail/(?P<pk>\d+)/$', DetailQuestionView.as_view(), name='question'),

    url(r'^create-question/$', CreateQuestionView.as_view(), name='create_question'),
    url(r'^edit-question/(?P<pk>\d+)/$', EditQuestionView.as_view(), name='edit_question'),
    url(r'^delete-question/(?P<pk>\d+)/$', DeleteQuestionView.as_view(), name='delete_question'),

    url(r'^answer-question/$', AnswerQuestionView.as_view(), name='answer_question'),

    url(r'^vote-object/$', VoteView.as_view(), name='vote_empty_object'),
    url(r'^vote-object/(?P<object_id>\d+)/(?P<object_type>\d+)/$', VoteView.as_view(), name='vote_object'),
    url(r'^comment-object/(?P<object_id>\d+)/(?P<object_type>\d+)/$', CommentView.as_view(), name='comment_object'),

    url(r'^question-distribution/(?P<quest_id>\d+)/$', QuestionDistributionView.as_view(), name='answers_distribution'),
    url(r'^upload-share-img/$', UploadShareImg.as_view(), name='upload_share_img'),

    url(r'^statistics/age-diagram/(?P<quest_id>\d+)/$', AgeStatisticView.as_view(), name='age_statistic'),
    url(r'^statistics/gender-diagram/(?P<quest_id>\d+)/$', GenderStatisticView.as_view(), name='gender_statistic'),

    url(r'^statistics/poll-date-diagram/(?P<quest_id>\d+)/$', PollDateStatisticView.as_view(),
        name='poll_date_statistic'),

    url(r'^statistics/countries-list/(?P<quest_id>\d+)/$', CountriesStatisticView.as_view(),
        name='countries_list'),

    url(r'^statistics/cities-list/$', CountryCityView.as_view(),
        name='country_cities_list'),

    url(r'^diagrams/statistics/cities-diagram/(?P<quest_id>\d+)/$', CitiesStatisticView.as_view(),
        name='cities_statistic'),

    url(r'^diagrams/statistics/general-info/(?P<quest_id>\d+)/$', GeneralQuestionInfoView.as_view(),
        name='general_info'),

    url(r'^city-suggestions/', GetCitySuggests.as_view(), name='city_suggests')

]
