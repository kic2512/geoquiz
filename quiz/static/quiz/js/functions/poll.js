/**
 * Created by max on 09.11.15.
 */

$(document).ready(function () {
    function setMetaTitleAndDesc (title, choiceText) {
        var shareText = "Я считаю, так: '" + choiceText + "'";
        $("meta[property='og\\:description']").attr("content", shareText);
        $("meta[property='og\\:title']").attr("content", title);
    }
    window.setMetaTitleAndDesc = setMetaTitleAndDesc;

    function getCitiesStatistic(url) {
        $.get(url)
            .success(function(response) {
                if ('data' in response && 'question_txt' in response) {

                    window.cityData = response['data'];

                    if (map.isCityView === true) {
                        drawCircles(window.cityData, window.queTitle);
                    }
                }
                else {
                    console.log(response);
                }
            });
    }

    function queryStatistic(queId, choiceText) {
        var url = '/question-distribution/' + queId + '/';
        $.get(url)
            .success(function(response) {
                if ('data' in response && 'question_txt' in response) {

                    window.queTitle = response['question_txt'];
                    setMetaTitleAndDesc(window.queTitle, choiceText);
                    window.coutryData = response['data'];

                    if (map.isCountryView === true) {
                        drawCircles(window.coutryData, window.queTitle);
                    }
                    getCitiesStatistic(url + '?cities=on');
                }
                else {
                    console.log(response);
                }
            });
    }
    window.queryStatistic = queryStatistic;

    function setPolls() {
        $('.poll-option').click(function (event) {
            var choiceId = $(this).data('id');
            var choiceText = $.trim($(this).html());

            var url = $(this).parent().data('url');
            var queId = $(this).parent().data('id');
            var params = {
                choice: choiceId
            };

            var isAnonym = !window.isAuthenticated;
            if (isAnonym) {
                localStorage.setItem(queId, choiceId);
                $("#anonym-alert").fadeIn();
                showStatistic(queId, choiceText);
            } else {
                $.post(url, params)
                    .done(function (response) {
                        if (response.status === 'OK') {
                            showStatistic(queId, choiceText);
                        }
                        else {
                            console.log(response.error);
                        }
                    })
                    .fail(function (jqXHR, textStatus) {
                        console.log('Проверьте сетевое подключение');
                    });
            }
        });
    }
    setPolls();
    window.setPolls = setPolls;

    function showStatistic(queId, choiceText) {
        $('.questions_popup').fadeOut(800);
        $('.lower-block').slideToggle(600);
        queryStatistic(queId, choiceText);
    }

    function setWonder () {
        var wonderUrl = $('.poll-wrapper').data('url');
        $.get(wonderUrl)
            .done(function (response) {
                if (response.status === 'OK') {
                    showPoll(response.data);
                }
                else {
                    console.log(response.error);
                }
            })
            .fail(function (xhr, textResponse) {
                console.log('Проверьте интернет-соединение');
            });
    }
    setWonder();
    window.setWonder = setWonder;
});