/**
 * Created by max on 06.10.15.
 */


$(document).ready(function () {
    function setAnswer () {
        var form = $('#answer-form');
        var inputs = $('#answer-form :input');
        var url = form.attr('action');

        form.submit(function (event) {
            event.preventDefault();

            var params = {choices: []};
            inputs.each(function() {
                if (this.name !== '') {
                    params[this.name] = $(this).val();
                }
            });

            $.post(
                url,
                params
            ).done(function (response) {
                if (response.status === 'OK') {
                    $('#answer-success').fadeIn().delay(5000).fadeOut(1000);
                    $('#answer-btn').fadeOut();
                }
                else {
                    console.log(response.error);
                }
            }).fail(function (jqXHR, textStatus) {
                console.log('Проверьте сетевое подключение');
            });
        });
    }
    setAnswer();
    window.setAnswer = setAnswer;
});