/**
 * Created by max on 05.10.15.
 */

$(document).ready(function () {
    function setCreateChoice () {
        $('#js-add-choice').click(function (event) {
            event.preventDefault();
            var lastId = parseInt($(this).attr('data-last-id')) + 1;
            var choiceTml = $('#choice-tml').html();
            var renderedHtml = _.template(choiceTml)({choiceId: lastId});
            $('#choices').append(renderedHtml);
            $(this).attr('data-last-id', lastId);
        });
    }
    setCreateChoice();
    window.setCreateChoice = setCreateChoice;

    function setCreateQuestion () {
        var form = $('#create-que-form');
        var url = form.attr('action');

        form.submit(function (event) {
            event.preventDefault();

            var inputs = $('#create-que-form :input');
            var params = {choices: []};
            inputs.each(function() {
                if (this.name !== '') {
                    if (this.name === 'choices')
                        params[this.name].push($(this).val());
                    else
                        params[this.name] = $(this).val();
                }
            });

            $.post(
                url,
                params
            ).done(function (response) {
                if (response.status === 'OK') {
                    if (response.data.url) {
                        console.log(response.data.url);
                        window.location = response.data.url;
                    }
                }
                else {
                    console.log(response.error);
                }
            }).fail(function (jqXHR, textStatus) {
                console.log('Проверьте сетевое подключение');
            });
        });
    }
    setCreateQuestion();
    window.setCreateQuestion = setCreateQuestion;

    function setTagsInput() {
        $('#bootstrapTagsInputForm')
            .find('[name="tags"]')
            .change(function (event) {
                $('#bootstrapTagsInputForm').formValidation('revalidateField', 'tags');
            })
            .end();
    }
    setTagsInput();
    window.setTagsInput = setTagsInput;
});