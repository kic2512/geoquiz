/**
 * Created by max on 06.10.15.
 */

$(document).ready(function () {
    function setVotes () {
        $('.rating-wrap').click(function (event) {
            event.preventDefault();

            var btn = $(this);
            var action = btn.attr('data-action');
            var dataDiv = btn.parent();
            var url = dataDiv.attr('data-url');

            $.post(url, {
                    action: action,
                    csrfmiddlewaretoken: $.cookie('csrftoken')
                }
            ).done(function (response) {
                if (response.status === 'OK') {
                    if (response.data.rating && response.data.value) {
                        btn.parent().find('.rating-number').html(response.data.rating);
                        btn.removeClass('hovered');
                        if (response.data.value > 0) {
                            btn.addClass('voted-up');
                        }
                        else {
                            btn.addClass('voted-down');
                        }
                        btn.siblings('.hovered').each(function () {
                            $(this).removeClass('hovered');
                        });
                    }
                }
                else {
                    console.log(response.error);
                }
            }).fail(function (xhr, textStatus) {
                console.log('Проверьте интернет-соединение');
            });
        });
    }
    setVotes();
    window.setVotes = setVotes;
});
