function Share (url, title, text, imgUrl) {
    this.url = url;
    this.title = title;
    this.text = text;
    this.imgUrl = imgUrl;

    this.popup = function (fullUrl) {
        window.open(fullUrl, '', 'toolbar=0,status=0,width=626,height=436');
    };
}

Share.prototype.vkontakte = function() {
    var url = 'http://vkontakte.ru/share.php?';
    url += 'url='          + encodeURIComponent(this.url);
    url += '&title='       + encodeURIComponent(this.title);
    url += '&description=' + encodeURIComponent(this.text);
    url += '&image='       + encodeURIComponent(this.imgUrl);
    url += '&noparse=true';
    this.popup(url);
};
Share.prototype.odnoklassniki = function() {
    var url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
    url += '&st.comments=' + encodeURIComponent(this.text);
    url += '&st._surl='    + encodeURIComponent(this.url);
    this.popup(url);
};
Share.prototype.facebook = function() {
    var url = 'http://www.facebook.com/sharer.php?s=100';
    url += '&p[title]='     + encodeURIComponent(this.title);
    url += '&p[summary]='   + encodeURIComponent(this.text);
    url += '&p[url]='       + encodeURIComponent(this.url);
    url += '&p[images][0]=' + encodeURIComponent(this.imgUrl);
    this.popup(url);
};
Share.prototype.twitter = function() {
    var url = 'http://twitter.com/share?';
    url += 'text='      + encodeURIComponent(this.title);
    url += '&url='      + encodeURIComponent(this.url);
    url += '&counturl=' + encodeURIComponent(this.url);
    this.popup(url);
};
Share.prototype.mailru = function() {
    var url = 'http://connect.mail.ru/share?';
    url += 'url='          + encodeURIComponent(this.url);
    url += '&title='       + encodeURIComponent(this.title);
    url += '&description=' + encodeURIComponent(this.text);
    url += '&imageurl='    + encodeURIComponent(this.imgUrl);
    this.popup(url)
};

$(document).ready(function () {
    function setShare () {
        var SD = $('#share-data');
        var share = new Share(SD.data('url'), SD.data('title'), SD.data('text'), SD.data('img'));
        $('.vk-share-link').click(function () {
            share.vkontakte();
        });
        $('.fb-share-link').click(function () {
            share.facebook();
        });
        $('.ok-share-link').click(function () {
            share.odnoklassniki();
        });
        $('.mailru-share-link').click(function () {
            share.mailru();
        });
    }
    setShare();
    window.setShare = setShare;
});