/**
 * Created by max on 11.10.15.
 */

$(document).ready(function () {
    function setComments () {
        var form = $('#add-comment-form');
        var url = form.attr('action');

        form.submit(function (event) {
            event.preventDefault();

            var inputs = $('#add-comment-form :input');
            var params = {};
            inputs.each(function() {
                if (this.name !== '') {
                    params[this.name] = $(this).val();
                }
            });

            $.post(
                url,
                params
            ).done(function (response) {
                if (response.status === 'OK') {
                    var comment = response.data.comment;
                    if (comment) {
                        var commentTml = $('#comment-tml').html();
                        var renderedHtml = _.template(commentTml)({comment: comment});
                        $('#comments').prepend(renderedHtml);
                        $('#while-empty').css({display: 'none'});
                    }
                    $('#id_text').val('');
                }
                else {
                    console.log(response.error);
                }
            }).fail(function (jqXHR, textStatus) {
                console.log('Проверьте сетевое подключение');
            });
        });
    }
    setComments();
    window.setComments = setComments;
});