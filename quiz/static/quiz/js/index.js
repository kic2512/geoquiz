$(document).ready(function () {
    function resizeContentDiv() {
        var height = $(window).height() - 150;
        if (height > 540) {
            $(".content-wrap").css({'height':height + 'px'});
        }
    }

    $('#see-question').click(function (event) {
        $(this).fadeOut('fast', function () {
            $('.lower-block').fadeOut('fast', function () {
                $('#next-question').fadeIn('fast', function () {
                    $('.questions_popup').fadeIn('fast');
                });
            });
        });
    });

    $('#next-question').click(function (event) {
        $('.questions_popup').fadeIn(800);
        $('.lower-block').slideToggle(600);
        setWonder();
        toggleMap("show");
        $("#anonym-alert").hide();

        clearMarkers();
    });

    $('#comments-question').click(function (event) {
       toggleMap("hide");
    });

    $('#statistics-question').click(function (event) {
        toggleMap("hide");
    });

    $('#skip-question').click(function (event) {
        $('.poll-content').hide().delay(1000).slideDown();
        $('.poll-place').html('');

        var loader = $('.loader');
        loader.slideDown(1);
        setWonder();
        loader.slideUp(1);
    });

    function toggleMap(action) {
        var locator = '#regions_div';
        if (action == "show") {
            $(locator).show();
        } else if (action == "hide") {
            $(locator).hide();
        }
    }

    function passToQuestionDetails() {
        $(".row-menu>.question-item>a").click(function (event) {
            event.preventDefault();

            var thisEl = $(this);
            var queUrl = thisEl.data('url');

            $.get(queUrl)
                .done(function (response) {
                    if (response.status === 'OK') {
                        showQuestion(response.data);
                    }
                    else {
                        console.log(response.error);
                    }
                })
                .fail(function (xhr, textResponse) {
                    console.log('Проверьте интернет-соединение');
                });
        });
    }
    passToQuestionDetails();
    window.setQuestionDetails = passToQuestionDetails;

    function passToQuestionList(queListUrl) {
        $.get(queListUrl)
            .done(function (response) {
                if (response.status === 'OK') {
                    showList(response.data);
                }
                else {
                    console.log(response.error);
                }
            })
            .fail(function (xhr, textStatus) {
                console.log('Проверьте интернет-соединение');
            });
    }
    var listDiv = $('.questions-list');
    listDiv.html('');

    var listUrl = listDiv.data('url');
    window.setQuestionList = passToQuestionList;

    function setListScroll() {
        var page = 2;
        $(window).scroll(function (event) {
            var winTop = $(window).scrollTop(),
                docHeight = $(document).height(),
                winHeight = $(window).height();

            var  scrollTrigger = 0.95;

            if  ((winTop / (docHeight - winHeight)) > scrollTrigger) {
                passToQuestionList(listUrl + '?page=' + page);
                page++;
            }
        });
    }
    window.setListScroll = setListScroll;

    $('#js-id-topic-filter').on('change', function (e) {
        var listUrl = $(this).children(':selected').attr('url');
        listDiv.html('');
        passToQuestionList(listUrl);
    });

    function setTagsSearch() {
        $('.search-tag').click(function (event) {
            var tagId = $(this).data('id');

            $(".contents").children().each(function() {
                $(this).hide();
            });
            $.get('/question-list/?tag=' + tagId)
                .done(function (response) {
                    if (response.status === 'OK') {
                        $('.questions-list').html('');
                        showList(response.data).done(function () {
                            $(".questions").fadeIn('fast');
                            $(window).scrollTop(1);
                        });
                    }
                    else {
                        console.log(response.error);
                    }
                })
                .fail(function (xhr, textStatus) {
                    console.log('Проверьте интернет-соединение');
                });

            var tab = $('a[href="#hot"]').parent();
            tab.siblings('.active').removeClass('active');
            tab.addClass('active');
            $(".questions").show();
        });
    }
    window.setTagsSearch = setTagsSearch;

    $('a[data-toggle="tab"]').click(function (event) {
        var target = $(this).attr("target");

        if (target == '.wonder') {
            resizeContentDiv();
        }
        else {
            if (target == '.questions') {
                listDiv.html('');
                $('#js-id-topic-filter').children('#topic-id-0').attr('selected','selected');

                var sorts = {
                    '#hot': 3,
                    '#best': 2,
                    '#new': 1
                };
                var sortOrder = sorts[$(this).attr('href')];
                passToQuestionList(listUrl + '?sort=' + sortOrder);
                setListScroll();
            }
        }
        $(".contents").children().each(function() {
            $(this).hide();
        });
        $(target).show();
    });

    resizeContentDiv();
});

