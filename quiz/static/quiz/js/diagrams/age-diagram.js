$(function () {

    $.getJSON('/statistics/age-diagram/27/', function (resp) {
        showDiagram(resp);
    });

    function showDiagram(resp) {
        $('#age-diagram').highcharts({
            title: {
                text: 'Распределение ответов по возрасту',
                x: -20 //center
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: getCategories(resp),
                title: {
                    text: 'Возраст проголосовавших'
                }
            },
            yAxis: {
                title: {
                    text: 'Количество проголосовавших'
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: '#808080'
                }]
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0
            },
            series: getSeries(resp)
        });
    }
});