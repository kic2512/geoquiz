$(function () {

    $.getJSON('/statistics/gender-diagram/27/', function(resp) {
        showDiagram(resp);
    });

    function showDiagram(resp) {
        $('#gender-diagram').highcharts({
            chart: {
                type: 'bar'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Распределения ответов по полу'
            },
            xAxis: {
                categories: getCategories(resp),
                title: {
                    text: 'Варианты ответов'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Количество проголосовавших'
                }
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            series: geGenders(resp)
        });
    }

    function geGenders(jsonResp) {
        var series = [];
        var index = 0;
        var colors = ['#90ed7d', '#8085e9', '#f15c80'];
        for (key in jsonResp["data"]["choices"]) {
            var ser = {};
            ser.name = [key];
            ser.data = jsonResp["data"]["choices"][key];
            ser.color = colors[index];
            series.push(ser);
            index = index + 1;
        }
        return series;
    }
});


