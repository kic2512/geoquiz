$(function () {
Highcharts.setOptions({
            lang: {
                loading: 'Загрузка...',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                exportButtonTitle: "Экспорт",
                printButtonTitle: "Печать",
                rangeSelectorFrom: "С",
                rangeSelectorTo: "По",
                rangeSelectorZoom: "Период",
                downloadPNG: 'Скачать PNG',
                downloadJPEG: 'Скачать JPEG',
                downloadPDF: 'Скачать PDF',
                downloadSVG: 'Скачать SVG',
                printChart: 'Напечатать график'
            }
    });
});


function getSeries(jsonResp) {
    var series = [];
    for (key in jsonResp["data"]["choices"]) {
        var ser = {};
        ser.name = jsonResp["data"]["choices"][key]["title"];
        ser.data = jsonResp["data"]["choices"][key]["counts"];
        ser.color = jsonResp["data"]["choices"][key]["colour"];
        series.push(ser);
    }
    return series;  
}

function getCategories(jsonResp) {
	return jsonResp["data"]["items"];
}

