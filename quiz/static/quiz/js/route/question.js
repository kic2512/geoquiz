/**
 * Created by max on 09.11.15.
 */

$(document).ready(function () {
    var fullHash = $(location).attr('hash');
    var parts = fullHash.split('=');
    var hash = parts[0];

    var actions = {
        '#q': function (queId) {
            if (typeof queId === 'undefined')
                return;

            $.get('/question-detail/' + queId + '/')
                .done(function (response) {
                    if (response.status === 'OK') {
                        showQuestion(response.data);
                        //$(".question-detail").show();
                        //$(".content-wrap").css({'height':100 + '%'});
                    }
                    else {
                        console.log(response.error);
                    }
                })
                .fail(function (xhr, textResponse) {
                    console.log('Проверьте интернет-соединение');
                });

            var tab = $('a[href="#hot"]').parent();
            tab.siblings('.active').removeClass('active');
            tab.addClass('active');
        },
        '#tag': function (tagId) {
            if (typeof tagId === 'undefined')
                return;

            $.get('/question-list/?tag=' + tagId)
                .done(function (response) {
                    if (response.status === 'OK') {
                        $('.questions-list').html('');
                        showList(response.data).done(function () {
                            $(window).scrollTop(1);
                        });
                    }
                    else {
                        console.log(response.error);
                    }
                })
                .fail(function (xhr, textStatus) {
                    console.log('Проверьте интернет-соединение');
                });

            var tab = $('a[href="#hot"]').parent();
            tab.siblings('.active').removeClass('active');
            tab.addClass('active');
        }
    };

    if (parts.length > 1) {
        var param = parts[1];
        if (typeof actions[hash] === 'function')
            actions[hash](param);
    }
    else {
        if (typeof actions[hash] === 'function')
            actions[hash]();
    }
});