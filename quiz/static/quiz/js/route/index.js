/**
 * Created by max on 09.11.15.
 */

$(document).ready(function () {
    var hash = $(location).attr('hash');

    var actions = {
        '': function () {
            $('.wonder').show();
        },
        '#hot': function () {
            $('a[href="'+ hash + '"]').trigger('click');
        },
        '#best': function () {
            $('a[href="'+ hash + '"]').trigger('click');
        },
        '#new': function () {
            $('a[href="'+ hash + '"]').trigger('click');
        },
        '#wonder': function () {
            $('a[href="'+ hash + '"]').trigger('click');
        },
        '#auth': function () {
            $('a[href="'+ hash + '"]').trigger('click');
        }
    };
    if (typeof actions[hash] === 'function')
        actions[hash]();
});