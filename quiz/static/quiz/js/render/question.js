/**
 * Created by max on 09.11.15.
 */

function showQuestion(data) {
    $(".questions").hide();
    var rateTml = $('#rate-tml').html();
    data['renderRate'] = _.template(rateTml);

    var commentTml = $('#comment-tml').html();
    data['renderComment'] = _.template(commentTml);

    var queTml = $('#question-tml').html();
    var renderedQueTml = _.template(queTml)(data);
    var detailDiv = $('.question-detail');

    detailDiv.html(renderedQueTml);

    $(window).unbind('scroll');
    $(window).scrollTop(0);

    setComments();
    setAnswer();
    setVotes();
    setTagsSearch();

    var queId = data.que.id;
    $('.edit-btn').click(function (event) {
        event.preventDefault();
        window.location.replace('edit-question/' + queId + '/');
    });
    $('.delete-btn').click(function (event) {
        event.preventDefault();

        if (confirm('Вы действительно хотите удалить опрос?')) {
            $.post('delete-question/' + queId + '/')
                .done(function (response) {
                    alert('Успешно удален');
                    window.location.replace('/');
                })
                .fail(function (xhr, textStatus) {
                    console.log('Не получилось удалить');
                });
        }
    });

    var choiceText = data.choice_answered_text || 'Не выбран';
    queryStatistic(queId, choiceText);

    $('.hide-btn').click(function (event) {
        $('.questions_popup').fadeOut('fast', function () {
            $('#next-question').fadeOut('fast', function () {
                $('.lower-block').fadeIn('fast', function () {
                    $('#see-question').fadeIn();
                });
            });
        });
    });

    var r = $.Deferred();
    $('.questions-list').fadeOut('fast', function () {
        $('.catty').fadeOut('fast', function () {
            detailDiv.fadeIn('fast', function () {
                r.resolve();
            });
        });
    });
    return r;
}