/**
 * Created by max on 09.11.15.
 */

function showPoll(data) {

    if ('error' in data) {
        var error = data.error;
        if (error === 403) {
            //$('a[href="#auth"]').trigger('click');
            return false;
        }
    }

    var pollTml = $('#poll-tml').html();
    var renderedPoll = _.template(pollTml)(data);
    $('.poll-place').html(renderedPoll);

    setPolls();
    $('.question-detail').hide();
    $('.questions-list').hide();
    $('.catty').hide();
}