/**
 * Created by max on 09.11.15.
 */

function showList(data) {
    var ques = data.questions;

    $('.catty').hide();
    var listDiv = $('.questions-list');

    var rateTml = $('#rate-tml').html();
    var listTml = $('#question-item-tml').html();

    if (ques.length == 0) {
        if (listDiv.html() === '')
            listDiv.html('<p class="empty-list-msg">Пока пусто.</p><p class="empty-list-msg">Добавьте новые опросы!</p>');
    }

    for (var I = 0; I < ques.length; I++) {
        var renderedRateTml = _.template(rateTml);
        var voteUrl = 'vote-object/' + ques[I].id + '/0/';
        var to_details_url = 'question-detail/' + ques[I].id + '/';

        var renderedListTml = _.template(listTml)({
            que: ques[I],
            voteUrl: voteUrl,
            renderRate: renderedRateTml,
            to_details_url: to_details_url
        });
        listDiv.append(renderedListTml);
    }
    setVotes();
    setQuestionDetails();

    var r = $.Deferred();
    $(".questions").fadeIn('fast', function () {
        $('.question-detail').fadeOut('fast', function () {
            listDiv.fadeIn('fast', function () {
                if (ques.length < 5) {
                    $('.catty').fadeIn('fast', function () {
                        r.resolve();
                    });
                }
                else {
                    r.resolve();
                }
            });
        });
    });
    return r;
}