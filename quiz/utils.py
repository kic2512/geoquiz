import random
import string
from collections import OrderedDict

from core.models import User
from core.utils import get_country, get_city_by_country
from quiz.countries_resolver import locations
from quiz.models import Choice, Question, Answer
from django.db.models import Count
from datetime import date, datetime
import pymorphy2

__author__ = 'kic'

colours = [
    '#FF0066',
    '#FF9933',
    '#FFFF00',
    '#99FF99',
    '#66CCFF',
    '#6100B4',
    '#e86af0',
    'cyan',
    'darkMagenta'
]

ANOMALY_HEIGHT_VAL = 60


def prepare_choices_struct(location, choices_pull, json_distribution, count_answers):
    """
    :param location:
    :param choices_pull:
    :param json_distribution:
    :param count_answers:

    :return: json_distribution = {
                'Russia':{
                    'statistics': {
                        choice1: 0.0,
                        choice2: 0.0,
                        choice3: 0.0,
                    },
                    lat: 54.23
                    lng: 69.23
                    name: 'Russia'
                }

            }

            count_answers = {
                'Russia': 10453,
            }
    """

    country_name = location['location_name']
    if not country_name:
        return None

    if location['coordinates'] is None:
        return None

    json_distribution[country_name] = {'statistics': {}}

    coordinates = location['coordinates']

    json_distribution[country_name]['lat'] = coordinates.x
    json_distribution[country_name]['lng'] = coordinates.y
    json_distribution[country_name]['name'] = country_name

    for choice in choices_pull:
        json_distribution[country_name]['statistics'][choice.text] = 0.0

    if country_name not in count_answers:
        count_answers[country_name] = 0.0

    count_answers[country_name] += location['choice_votes']


def get_percent(country_choice, total_count):
    people_count = country_choice['choice_count_inside_country']  # people who choose this choice in this country
    return round(people_count / total_count * 100, 1) if total_count else 0


def random_seq(length):
    symbols = string.ascii_letters + string.digits
    return ''.join(random.choice(symbols) for _ in range(length))


def get_countries_distribution(question_id, choices_pull):
    country_choices_dict = Choice.objects.filter(question_id=question_id).values(
            'answer__user__country__international_name').annotate(
            choice_count_inside_country=Count('answer__user__country__international_name')).values(
            'answer__user__country__international_name',
            'text',
            'choice_count_inside_country',
            'answer__user__country__coordinates')

    fields = {
        'location_name': 'answer__user__country__international_name',
        'choice_votes': 'choice_count_inside_country',
        'coordinates': 'answer__user__country__coordinates'
    }

    return make_distribution(country_choices_dict, choices_pull, fields)


def get_cities_distribution(question_id, choices_pull):
    city_choices_dict = Choice.objects.filter(question_id=question_id).values(
            'answer__user__city__international_name').annotate(
            choice_count_inside_city=Count('answer__user__city__international_name')).values(
            'answer__user__city__international_name',
            'text',
            'choice_count_inside_city',
            'answer__user__city__coordinates')
    fields = {
        'location_name': 'answer__user__city__international_name',
        'choice_votes': 'choice_count_inside_city',
        'coordinates': 'answer__user__city__coordinates'
    }

    return make_distribution(city_choices_dict, choices_pull, fields)


def make_distribution(location_data, choices_pull, fields):
    json_distribution = {}
    count_answers_in_location = {}

    location_name_field = fields['location_name']
    coordinates_field = fields['coordinates']
    choice_votes_field = fields['choice_votes']

    for location_choice in location_data:
        location = {
            'location_name': location_choice[location_name_field],
            'coordinates': location_choice[coordinates_field],
            'choice_votes': location_choice[choice_votes_field],
        }

        prepare_choices_struct(location, choices_pull, json_distribution, count_answers_in_location)

    for location_choice in location_data:

        location_name = location_choice[location_name_field]
        if not location_name:
            continue

        choice_text = location_choice['text']

        if location_name not in count_answers_in_location:
            continue

        total_count = count_answers_in_location[location_name]

        json_distribution[location_name]['statistics'][choice_text] = location_choice[choice_votes_field]

        if 'total' not in json_distribution[location_name]:
            json_distribution[location_name]['total'] = total_count

    return json_distribution


def get_age_statistic(question_id, city_name=None, country_name=None):
    """

    :param country_name:
    :param city_name:
    :param question_id:
    :return: {
            items: [15, 20, 25, 30, 35, 40, 45, 50],
            choices: {
                124: {
                    counts: [31, 13, 23, 33, 12, 32, 21, 31],
                    title: 'Variant 1',
                    colour: '#333'
                }
                125: {
                    counts: [11, 23, 13, 23, 21, 3, 11, 22],
                    title: 'Variant 2',
                    colour: '#FFF',
                }
            }
    }
    """

    color_idx = 0

    ages_coordinate_line = [15, 20, 25, 30, 35, 40, 45, 50, 55, 60]  # 0-ой элемент - меньше 15 лет, 1-й - от 15 до 20
    # , последний - больше 60

    points_count = len(ages_coordinate_line)

    query = Answer.objects.filter(choice__question_id=question_id)

    country = None
    if country_name:
        country = get_country(country_name)
        if country:
            query = query.filter(user__country=country)

    if city_name and country:
        city = get_city_by_country(city_name, country)
        if city:
            query = query.filter(user__city=city)

    answers = query.values('choice__id', 'choice__text', 'user__date_of_birth')

    data = {
        'items': ages_coordinate_line,
        'choices': {}
    }

    for answer in answers:
        choice_id = answer['choice__id']
        if choice_id not in data['choices']:
            data['choices'][choice_id] = {
                'counts': [0] * points_count,
                'title': answer['choice__text'],
                'colour': colours[color_idx % len(colours)],
            }

            color_idx += 1

        if answer['user__date_of_birth']:
            age = get_age_by_date(answer['user__date_of_birth'])
            if age:
                idx = bin_search(ages_coordinate_line, 0, points_count - 1, age)
                data['choices'][choice_id]['counts'][idx] += 1

    i = 0
    for item in data['items']:
        data['items'][i] = '{0} лет'.format(item)
        i += 1

    data['items'][0] = 'менее {0}'.format(data['items'][0])
    last = len(data['items']) - 1
    data['items'][last] = 'более {0}'.format(data['items'][last])

    return data


def bin_search(arr, l, r, x):
    if r <= l:
        return r

    mid = (r + l) // 2

    if x <= arr[mid]:
        return bin_search(arr, l, mid, x)
    else:
        return bin_search(arr, mid + 1, r, x)


def get_age_by_date(born_date):
    try:
        today = date.today()
        return today.year - born_date.year - ((today.month, today.day) < (born_date.month, born_date.day))
    except Exception:
        return None


def get_gender_statistic(question_id, city_name=None, country_name=None):
    query = Answer.objects.filter(choice__question_id=question_id)

    country = None
    if country_name:
        country = get_country(country_name)
        if country:
            query = query.filter(user__country=country)

    if city_name and country:
        city = get_city_by_country(city_name, country)
        if city:
            query = query.filter(user__city=city)

    answers = query.values('choice__id', 'choice__text', 'user__gender')

    gender_map = {
        'M': 'мужской',
        'F': 'женский',
        'N': 'не указан',
    }

    choices_list = []
    for answer in answers:
        text = answer['choice__text']
        if text not in choices_list:
            choices_list.append(text)

    points_count = len(choices_list)

    male = 'мужской'
    female = 'женский'
    unknown = 'не указан'

    data = {
        'items': choices_list,
        'choices': {
            male: [0] * points_count,
            female: [0] * points_count,
            unknown: [0] * points_count
        }
    }

    for answer in answers:
        gender_key = answer['user__gender']
        gender = gender_map[gender_key]

        idx = choices_list.index(answer['choice__text'])

        data['choices'][gender][idx] += 1

    return data


def get_poll_date_statistic(question_id, city_name=None, country_name=None):
    color_idx = 0

    query = Answer.objects.filter(choice__question_id=question_id)

    country = None
    if country_name:
        country = get_country(country_name)
        if country:
            query = query.filter(user__country=country)

    if city_name and country:
        city = get_city_by_country(city_name, country)
        if city:
            query = query.filter(user__city=city)

    answers = query.order_by('create_date').values('choice__id', 'choice__text', 'create_date')

    date_set = OrderedDict()

    for answer in answers:
        format_date = datetime.strftime(answer['create_date'], '%d.%m.%Y')
        date_set[format_date] = 0

    data_list = [k for k in date_set]

    data = {
        'items': data_list,
        'choices': {}
    }

    for answer in answers:

        choice_id = answer['choice__id']

        if choice_id not in data['choices']:
            data['choices'][choice_id] = {
                'counts': [0] * len(date_set),
                'title': answer['choice__text'],
                'colour': colours[color_idx % len(colours)],
            }

            color_idx += 1

        format_date = datetime.strftime(answer['create_date'], '%d.%m.%Y')
        idx = data_list.index(format_date)

        data['choices'][choice_id]['counts'][idx] += 1

    return data


def find_object(struct, choice_id, country, key):
    for obj in struct:
        if obj['id'] == choice_id and obj[key] == country:
            return obj


def get_countries_statistic(question_id):
    country_choices_dict = Choice.objects.filter(question_id=question_id).values(
            'answer__user__country__international_name').annotate(
            choice_count_inside_country=Count('answer__user__country__international_name')).values(
            'answer__user__country__international_name',
            'id',
            'text',
            'choice_count_inside_country')

    countries = []
    for country in country_choices_dict:
        name = country['answer__user__country__international_name']
        if name not in countries:
            countries.append(name)

    data = []
    for country in countries:
        data.append({'title': country})

    return data


def get_cities_statistic(question_id, country_name):
    country = get_country(country_name)
    city_ans_dict = Answer.objects.filter(choice__question_id=question_id, user__country=country). \
        values('user__city__international_name'). \
        annotate(choice_count_inside_city=Count('user__city__international_name')). \
        values(
            'user__city__international_name',
            'choice_count_inside_city',
    )

    items = []
    for city in city_ans_dict:
        items.append({
            'title': city['user__city__international_name'],
            'counts': city['choice_count_inside_city']
        })

    data = {
        'items': items
    }
    return data


def get_sharing_text(user, question):
    if not user.country:
        return None

    user_choice = Choice.objects.filter(question=question, answer__user=user).first()
    if not user_choice:
        return None

    country_choices_dict = Answer.objects.filter(choice=user_choice).values('user__country__id').annotate(
        choice_count_inside_country=Count('user__country__id')).values('user__country__id',
                                                                       'user__country__cyrillic_name',
                                                                       'choice_count_inside_country')

    country_total = Answer.objects.filter(choice__question=question).values('user__country__cyrillic_name').annotate(
            answers_count_inside_country=Count('user__country__cyrillic_name')).values(
            'user__country__id',
            'answers_count_inside_country')

    morph = pymorphy2.MorphAnalyzer()

    total_dict = {}
    for country_choice in country_choices_dict:
        country_id = country_choice['user__country__id']
        country_choice_val = country_choice['choice_count_inside_country']
        if country_id not in total_dict:
            total_answered = find_total(country_id, country_total)
            total_dict[country_id] = total_answered

        else:
            total_answered = total_dict[country_id]

        percent = 0
        if total_answered > 0:
            percent = int(country_choice_val * 100.0 / total_answered)

        exception_cnt = 0
        if percent > ANOMALY_HEIGHT_VAL:
            full_name = ''
            for part in country_choice['user__country__cyrillic_name'].split(' '):
                try:
                    cyr_name = morph.parse(part)[0].inflect({'gent'}).word
                except Exception:
                    exception_cnt += 1
                    break
                if full_name == '':
                    full_name += cyr_name
                else:
                    full_name += ' {0}'.format(cyr_name)

            if exception_cnt == 0:
                text = 'С Вами согласно {0}% пользователей из {1}'.format(percent, full_name.title())
                return text

    total_count = Answer.objects.filter(choice__question=question).count()
    choice_count = Answer.objects.filter(choice=user_choice).count()

    if total_count:
        percent = int(choice_count * 100.0 / total_count)
        text = 'С Вами согласно {0}% пользователей'.format(percent)
        return text


def find_total(id, country_total):
    for country in country_total:
        if id == country['user__country__id']:
            return country['answers_count_inside_country']


def get_world_statistic(choices_pull):
    """

    :param choices_pull:
    :return:
            [
               {
                    choice_id: id,
                    title: title,
                    count: 2312,
               },
               {

               }
            ]
    """
    statistic = []
    for choice in choices_pull:
        count = Answer.objects.filter(choice=choice).count()
        statistic.append({
            'choice_id': choice.id,
            'title': choice.text,
            'count': count
        })

    return statistic
