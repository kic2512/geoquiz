from django import template

__author__ = 'max'

register = template.Library()


@register.filter(name='add_class')
def add_class(bound, classes=''):
    """ фильтр для добавления класса form-control для input-ов """

    widget = bound.field.widget
    widget.attrs['class'] = 'form-control ' + classes

    # ограничение на три строки для textarea
    widget.attrs['rows'] = 3
    return bound


@register.filter(name='dec')
def dec(number, right=1):
    return number - right


@register.filter(name='inc')
def inc(number, right=1):
    return number + right


@register.filter(name='range')
def dec(number):
    return [i for i in range(number)]


@register.filter(name='index')
def index(obj, idx):
    if len(obj) > idx:
        return obj[idx]
    return None

