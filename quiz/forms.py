from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from quiz.models import Question, Choice, Answer, Vote, Comment, Tag

__author__ = 'max'


class CreateQuestionForm(forms.ModelForm):
    def __init__(self, author, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.author = author

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.author = self.author
        if commit:
            instance.save()
        return instance

    class Meta:
        model = Question
        fields = ('title', 'text', 'topic')


class UpdateQuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('title', 'text', 'topic')


class CreateChoiceForm(forms.ModelForm):
    def __init__(self, question, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.question = question

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.question = self.question
        if commit:
            instance.save()
        return instance

    class Meta:
        model = Choice
        fields = ('text',)


class CreateVoteForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.has_vote = False
        self.vote_id = None

    def clean(self):
        cleaned_data = super().clean()
        object_id = cleaned_data['object_id']
        object_type = cleaned_data['object_type']
        vote = Vote.objects.filter(user=self.user, object_id=object_id, object_type=object_type).first()
        if vote:
            self.has_vote = vote.is_active
            self.vote_id = vote.id
        return cleaned_data

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.user = self.user
        if commit:
            if self.has_vote:
                instance.is_active = False
            if self.vote_id:
                instance.id = self.vote_id
            instance.save()
        return instance

    class Meta:
        model = Vote
        fields = ('object_id', 'object_type')


class CreateCommentForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.author = self.user
        if commit:
            instance.save()
        return instance

    class Meta:
        model = Comment
        fields = ('object_id', 'object_type', 'text')


class CreateAnswerForm(forms.ModelForm):
    def __init__(self, choice, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.choice = choice

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.user = self.user
        instance.choice = self.choice
        if commit:
            instance.save()
        return instance

    class Meta:
        model = Answer
        fields = ()