from __future__ import division

import json as json_lib
from datetime import datetime
import os
from urllib.parse import unquote

from core.models import User, Country, City
from core.utils import get_country
from django.core.urlresolvers import reverse
from django.db.models import Count, Q
from django.http import HttpResponseBadRequest, HttpResponseForbidden, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from geoquiz import settings
from quiz.forms import CreateQuestionForm, CreateChoiceForm, CreateVoteForm, CreateAnswerForm, CreateCommentForm, \
    UpdateQuestionForm
from utils.access import LoginRequiredMixin
from quiz.models import Question, Choice, Vote, Answer, VoteManager, Comment, CommentManager, Tag
from utils.json_views import JsonCreateView, JsonDetailView, JsonListView, JsonUpdateView, JsonDeleteView, JsonView
from math import ceil
from django.core.exceptions import ObjectDoesNotExist
from quiz.utils import prepare_choices_struct, get_percent, random_seq, get_countries_distribution, \
    get_cities_distribution, get_age_statistic, get_gender_statistic, get_poll_date_statistic, get_countries_statistic, \
    get_cities_statistic, get_sharing_text, get_world_statistic
from base64 import b64decode

STANDARD_SHARING_TEXT = 'GeoPoll - Узнай что думают люди в разных уголках земли'


class IndexView(TemplateView):
    http_method_names = ['get']
    template_name = 'quiz/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        context['questions'] = Question.objects.filter(is_active=True) \
            .select_related('author').order_by('-rating')

        context['question_class'] = Question
        context['index'] = True
        return context


class QuestionListView(JsonListView):
    http_method_names = ['get']
    model = Question

    SORTS = {
        1: '-pub_date',
        2: '-rating',
        3: '-comments_cnt',
    }

    GET_MY_QUESTIONS = 1
    GET_MY_ANSWERED_QUESTIONS = 2

    def get(self, request, *args, **kwargs):
        try:
            self.page = int(request.GET.get('page', 1))
            self.question_cnt = int(request.GET.get('questions_cnt', 100))
            self.sort_order = int(request.GET.get('sort', 2))
            self.extra = int(request.GET.get('extra', 0))
        except (ValueError, TypeError):
            self.page = 1
            self.question_cnt = 100
            self.sort_order = 2

        return super().get(request, *args, **kwargs)

    def filter_by_topic(self, questions):
        if 'topic' in self.request.GET:
            try:
                topic_numb = int(self.request.GET['topic'])
                if Question.ALL < topic_numb <= Question.OTHER:
                    return questions.filter(topic=topic_numb)
            except (TypeError, ValueError):
                pass
        return questions

    def filter_by_tags(self, questions):
        if 'tag' in self.request.GET:
            try:
                tag_id = int(self.request.GET['tag'])
                return questions.filter(tag__id__exact=tag_id)
            except (TypeError, ValueError):
                pass
        return questions

    def get_questions(self, start_pos, end_pos):
        sort = self.SORTS.get(self.sort_order, '-rating')
        if self.extra == self.GET_MY_QUESTIONS and not self.request.user.is_anonymous():
            questions = Question.objects.filter(is_active=True, author=self.request.user).select_related(
                    'author').order_by(sort)
        elif self.extra == self.GET_MY_ANSWERED_QUESTIONS and not self.request.user.is_anonymous():
            questions = Question.objects.filter(is_active=True, choice__answer__user=self.request.user).select_related(
                    'author').order_by(sort)
        else:
            questions = Question.objects.filter(is_active=True).select_related('author').order_by(sort)

        questions = self.filter_by_topic(questions)
        questions = self.filter_by_tags(questions)
        questions = questions[start_pos:end_pos]

        data = []
        user = self.request.user
        for que in questions:
            que_data = que.as_dict()
            que_data['has_voted'] = None
            que_data['has_answer'] = None
            que_data['vote_url'] = reverse('quiz:vote_object', args=[que_data['id'], Vote.TYPE_QUESTION])
            if user.is_authenticated():
                que_data['has_voted'] = Vote.objects.question_is_voted(que, user)
                que_data['has_answer'] = Answer.objects.question_is_answered(que, user)
            data.append(que_data)
        return data

    def get_json_data(self, **kwargs):
        end_pos = self.page * self.question_cnt
        start_pos = end_pos - self.question_cnt

        data = dict(page=self.page)
        data['questions'] = self.get_questions(start_pos, end_pos)
        data['pages_cnt'] = ceil(Question.objects.count() / self.question_cnt)

        json = super().get_json_data(**kwargs)
        json['data'] = data
        return json


class WonderQuestionView(JsonDetailView):
    http_method_names = ['get']
    model = Question

    def get_object(self, queryset=None):
        user = self.request.user
        queryset = Question.objects.annotate(num_choices=Count('choice')) \
            .filter(num_choices__gt=0).order_by('?')

        if user.is_anonymous():
            return Question.objects.order_by('?').first()
        else:
            que = queryset.first()
            num_que = len(queryset)
            count = 0
            while Answer.objects.filter(choice__question=que, user=user).exists():
                que = queryset.first()
                if count > num_que:
                    return None
                count += 1
            return que

    def get_choices(self):
        result = []
        choices = Choice.objects.filter(question=self.object, is_active=True)
        for choice in choices:
            choice_data = choice.as_dict()
            result.append(choice_data)
        return result

    def get_json_data(self, **kwargs):
        if self.object is not None:
            json = super().get_json_data(**kwargs)
            json['data'] = self.object.as_dict()
            json['data']['choices'] = self.get_choices()
            json['data']['vote_url'] = reverse('quiz:vote_object', args=[self.object.id, Vote.TYPE_QUESTION])
        else:
            json = super(WonderQuestionView, self).get_error_data(**kwargs)
            json['error'] = "К сожалению, вопросы закончились. Попробуйте позже."
        return json


class DetailQuestionView(JsonDetailView):
    http_method_names = ['get']
    model = Question

    def get_choices(self, user):
        result = []
        choices = Choice.objects.filter(question=self.object, is_active=True)
        for choice in choices:
            choice_data = choice.as_dict()
            if user.is_authenticated():
                choice_data['has_voted'] = Answer.objects.filter(user=user, choice=choice).exists()
            result.append(choice_data)
        return result

    def get_comments(self):
        comments = Comment.objects.get_question_comments(self.object).order_by('-pub_date')
        return self.objects_to_dict(comments)

    def get_json_data(self, **kwargs):
        user = self.request.user

        data = dict(que=self.object.as_dict())
        data['que']['choices'] = self.get_choices(user)
        data['que']['comments'] = self.get_comments()
        data['que']['tags'] = self.objects_to_dict(self.object.tag_set.all())
        data['que']['is_author'] = user == self.object.author
        data['que']['is_anonymous'] = user.is_anonymous()
        data['que']['current_user_avatar'] = ''
        if not user.is_anonymous():
            data['que']['current_user_avatar'] = user.avatar.url

        data['que']['has_voted'] = None
        data['que']['choice_answered_id'] = None
        data['que']['choice_answered_text'] = None

        if user.is_authenticated():
            answer = Answer.objects.filter(choice__question=self.object, user=user, is_active=True).first()
            if answer is not None:
                data['que']['choice_answered_id'] = answer.choice.id
                data['que']['choice_answered_text'] = answer.choice.text
            data['que']['has_voted'] = Vote.objects.question_is_voted(self.object, self.request.user)

        data['que']['vote_url'] = reverse('quiz:vote_object', args=[self.object.id, Vote.TYPE_QUESTION])
        data['que']['comment_url'] = reverse('quiz:comment_object', args=[self.object.id, Comment.TYPE_QUESTION])

        json = super().get_json_data(**kwargs)
        json['data'] = data
        return json


class VoteView(LoginRequiredMixin, JsonCreateView):
    http_method_names = ['post']
    model = Vote
    form_class = CreateVoteForm
    success_url = None

    def post(self, request, *args, **kwargs):

        object_id = kwargs.get('object_id')
        object_type = kwargs.get('object_type')

        if not object_id or not object_type:
            return HttpResponseBadRequest()

        try:
            self.object_id = int(object_id)
            self.object_type = int(object_type)
            self.voted_object = VoteManager.get_object(self.object_id, self.object_type)
        except (ValueError, ObjectDoesNotExist):
            return HttpResponseBadRequest()
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['data'] = {
            'object_id': self.object_id,
            'object_type': self.object_type,
        }
        return kwargs

    def form_valid(self, form):

        if not form.has_vote:
            self.voted_object.rating += 1
            self.is_increace = True
        else:
            self.voted_object.rating -= 1
            self.is_increace = False
        self.voted_object.save(update_fields=['rating'])
        return super().form_valid(form)

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)
        json['data'].update({
            'rating': self.voted_object.rating,
            'has_increase': self.is_increace
        })
        return json


class CreateQuestionView(LoginRequiredMixin, JsonCreateView):
    http_method_names = ['post']
    model = Question
    form_class = CreateQuestionForm
    template_name = 'quiz/create_question.html'

    def post(self, request, *args, **kwargs):
        try:
            request.POST = json_lib.loads(request.body.decode("utf-8"))
        except Exception:
            return self.json_error('Неверный запрос')
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['author'] = self.request.user
        return kwargs

    def get_success_url(self):
        return reverse('quiz:index')

    def form_valid(self, form):
        choices = self.request.POST.get('choices', [])
        if len(choices) < 2:
            return self.json_error('Необходимо не меньше двух вариантов ответа')

        question_form = super().form_valid(form)

        tags = self.request.POST.get('tags', '').split(',')
        for tag_name in tags:
            if tag_name:
                tag, _ = Tag.objects.get_or_create(tag=tag_name)
                self.object.tag_set.add(tag)

        for choice_text in choices:
            choice_form = CreateChoiceForm(question=self.object, data={'text': choice_text})
            if choice_form.is_valid():
                choice_form.save()

        return question_form


class AnswerQuestionView(LoginRequiredMixin, JsonCreateView):
    http_method_names = ['post']
    model = Answer
    form_class = CreateAnswerForm

    def post(self, request, *args, **kwargs):
        choice_id = self.request.POST.get('choice', None)

        if not choice_id:
            return HttpResponseBadRequest()
        try:
            choice_id = int(choice_id)
        except ValueError:
            return HttpResponseBadRequest()
        self.choice = get_object_or_404(Choice, id=choice_id)

        if Answer.objects.filter(choice__question=self.choice.question, user=request.user).exists():
            return self.json_error('Вы уже отвечали на этот вопрос')

        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['choice'] = self.choice
        return kwargs

    def get_success_url(self):
        que_id = self.choice.question.id
        return reverse('quiz:question', args=[que_id])


class CommentView(LoginRequiredMixin, JsonCreateView):
    http_method_names = ['post']
    model = Comment
    form_class = CreateCommentForm

    def post(self, request, *args, **kwargs):
        object_id = kwargs.get('object_id')
        object_type = kwargs.get('object_type', 0)

        text = request.POST.get('text', '')

        if text == '':
            json = self.get_error_data()
            json['error'] = 'Введите текст'
            return JsonResponse(json)

        if not object_id or not object_type:
            json = self.get_error_data()
            json['error'] = 'Неверный запрос'
            return JsonResponse(json)

        try:
            self.object_id = int(object_id)
            self.object_type = int(object_type)
            self.commented_object = CommentManager.get_object(self.object_id, self.object_type)
            self.text = text
        except (ValueError, ObjectDoesNotExist):
            json = self.get_error_data()
            json['error'] = 'Неверный запрос'
            return JsonResponse(json)

        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['data'] = {
            'object_id': self.object_id,
            'object_type': self.object_type,
            'text': self.text
        }
        return kwargs

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)
        json['data'].update({
            'comment': self.object.as_dict()
        })
        return json

    def form_valid(self, form):
        self.commented_object.comments_cnt += 1
        self.commented_object.save(update_fields=['comments_cnt'])
        return super().form_valid(form)


class QuestionDistributionView(JsonView):
    http_method_names = ['get']
    model = Question

    def get_json_data(self, **kwargs):

        json = super().get_json_data(**kwargs)

        question_id = kwargs['quest_id']

        question = Question.objects.filter(id=question_id).first()

        if not question:
            json['status'] = 'FAIL'
            json['error'] = 'Question does not exists'
            return json

        json['question_txt'] = question.title

        choices_pull = Choice.objects.filter(question_id=question_id)

        if 'cities' in self.request.GET:
            json['data'] = get_cities_distribution(question_id, choices_pull)
        else:
            json['data'] = get_countries_distribution(question_id, choices_pull)

        json['world_statistic'] = get_world_statistic(choices_pull)

        if not self.request.user.is_anonymous():
            sharing_text = get_sharing_text(self.request.user, question)
            if not sharing_text:
                sharing_text = STANDARD_SHARING_TEXT
        else:
            sharing_text = STANDARD_SHARING_TEXT

        json['sharing_text'] = sharing_text

        return json


class EditQuestionView(LoginRequiredMixin, JsonUpdateView):
    http_method_names = ['get', 'post']
    template_name = 'quiz/create_question.html'
    form_class = UpdateQuestionForm
    model = Question

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.user != self.object.author:
            return HttpResponseForbidden()
        return super(EditQuestionView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('quiz:index')

    def get_context_data(self, **kwargs):
        context = super(EditQuestionView, self).get_context_data(**kwargs)
        context['update'] = True
        context['instance_tags'] = ','.join([tag.tag for tag in self.object.tag_set.all()])
        context['instance_choices'] = self.object.choice_set.filter(is_active=True)
        return context

    def form_valid(self, form):
        question_form = super().form_valid(form)

        tags = self.request.POST.get('tags').split(',')
        instance_tags = [tag.tag for tag in self.object.tag_set.all()]

        for tag_name in tags:
            if tag_name not in instance_tags:
                tag, _ = Tag.objects.get_or_create(tag=tag_name)
                self.object.tag_set.add(tag)
            else:
                instance_tags.remove(tag_name)

        for tag_name in instance_tags:
            tag = Tag.objects.get(tag=tag_name)
            self.object.tag_set.remove(tag)

        old_choices = self.request.POST.getlist('old_choices[]', [])
        choices = self.request.POST.getlist('choices[]', [])
        instance_choices = self.object.choice_set.filter(is_active=True)

        for choice, new_text in zip(instance_choices, old_choices):
            choice.text = new_text

        for choice_text in choices:
            choice_form = CreateChoiceForm(question=self.object, data={'text': choice_text})
            if choice_form.is_valid():
                choice_form.save()

        return question_form


class DeleteQuestionView(LoginRequiredMixin, JsonDeleteView):
    http_method_names = ['post']
    model = Question

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.user != self.object.author:
            return HttpResponseForbidden()
        return super(DeleteQuestionView, self).dispatch(request, *args, **kwargs)


class UploadShareImg(LoginRequiredMixin, JsonView):
    http_method_names = ['post']
    upload_path = 'share'

    def parse_img_type(self, img_type):
        parts = img_type.split(';')
        if len(parts) > 1:
            mime_type, encode_type = parts[0], parts[1]
            parts = mime_type.split('/')
            if len(parts) > 1:
                is_img, extension = parts[0], parts[1]
                if is_img == 'image':
                    return extension, encode_type
                else:
                    raise ValueError('Invalid img_type')
            else:
                raise KeyError('Invalid img_type')
        else:
            raise KeyError('Invalid img_type')

    def save_picture(self, user, img_data, img_type):
        extension, encode_type = self.parse_img_type(img_type)
        pic_title = '%s_%s.%s' % (user.get_short_name(), random_seq(10), extension)
        pic_path = os.path.join(settings.MEDIA_ROOT, self.upload_path, pic_title)
        pic_dir_path = os.path.join(settings.MEDIA_ROOT, self.upload_path)

        if not os.path.exists(pic_dir_path):
            os.makedirs(pic_dir_path)

        if encode_type == 'base64':
            with open(pic_path, 'wb') as dest_file:
                dest_file.write(b64decode(img_data.encode('ascii')))
        else:
            raise ValueError('Unsupported encode_type')
        return os.path.join(self.upload_path, pic_title)

    def post(self, request, *args, **kwargs):
        if 'data' in request.POST and 'dataType' in request.POST:
            img_data = request.POST['data']
            img_type = request.POST['dataType']
            try:
                pic_path = self.save_picture(request.user, img_data, img_type)
                pic_url = 'http://%s/%s' % (request.META['HTTP_HOST'], pic_path)
                return super().post(request, pic_url=pic_url, *args, **kwargs)
            except Exception as e:
                if settings.DEBUG:
                    return super().json_error(str(e))
                else:
                    return super().json_error('Upload image error')
        else:
            return super().json_error('No share_img_url in request')

    def get_json_data(self, **kwargs):
        json = super(UploadShareImg, self).get_json_data(**kwargs)
        json['data'].update({
            'img_url': kwargs['pic_url']
        })
        return json


class AgeStatisticView(JsonView):
    http_method_names = ['get']

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)

        quest_id = kwargs.get('quest_id')
        city_name = self.request.GET.get('city')
        if city_name:
            city_name = unquote(city_name)

        country_name = self.request.GET.get('country')
        if country_name:
            country_name = unquote(country_name)

        quest = Question.objects.filter(id=quest_id).first()

        if not quest or not quest_id:
            json_error = self.get_error_data()
            json_error['error'] = 404
            json_error['message'] = 'Вопрос не найден'

            return JsonResponse(json_error)

        json['data'] = get_age_statistic(quest_id, city_name, country_name)
        return json


class GenderStatisticView(JsonView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        quest_id = kwargs.get('quest_id')
        city_name = self.request.GET.get('city')
        if city_name:
            city_name = unquote(city_name)

        country_name = self.request.GET.get('country')
        if country_name:
            country_name = unquote(country_name)

        quest = Question.objects.filter(id=quest_id).first()

        if not quest or not quest_id:
            json_error = self.get_error_data()
            json_error['error'] = 404
            json_error['message'] = 'Вопрос не найден'

            return JsonResponse(json_error)

        self.statistic = get_gender_statistic(quest_id, city_name, country_name)
        return super().get(request, *args, **kwargs)

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)
        json['data'] = self.statistic
        return json


class PollDateStatisticView(JsonView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        quest_id = kwargs.get('quest_id')
        city_name = self.request.GET.get('city')
        if city_name:
            city_name = unquote(city_name)

        country_name = self.request.GET.get('country')
        if country_name:
            country_name = unquote(country_name)

        quest = Question.objects.filter(id=quest_id).first()

        if not quest:
            json_error = self.get_error_data()
            json_error['error'] = 404
            json_error['message'] = 'Вопрос не найден'

            return JsonResponse(json_error)

        self.statistic = get_poll_date_statistic(quest_id, city_name, country_name)
        return super().get(request, *args, **kwargs)

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)
        json['data'] = self.statistic
        return json


class CountriesStatisticView(JsonView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        question_id = kwargs.get('quest_id')

        quest = Question.objects.filter(id=question_id).first()

        if not quest or not question_id:
            json_error = self.get_error_data()
            json_error['error'] = 404
            json_error['message'] = 'Вопрос не найден'
            return JsonResponse(json_error)

        self.statistic = get_countries_statistic(question_id)

        return super().get(request, *args, **kwargs)

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)
        json['data'] = self.statistic
        return json


class CitiesStatisticView(JsonView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):

        question_id = kwargs.get('quest_id')
        country_name = self.request.GET.get('country')
        if country_name:
            country_name = unquote(country_name, 'utf-8')

        quest = Question.objects.filter(id=question_id).first()

        country = get_country(country_name)
        is_country_exist = User.objects.filter(country=country).exists()

        if not quest or not question_id:
            json_error = self.get_error_data()
            json_error['error'] = 404
            json_error['message'] = 'Вопрос не найден'
            return JsonResponse(json_error)

        if not is_country_exist:
            json_error = self.get_error_data()
            json_error['error'] = 404
            json_error['message'] = 'Страна не найдена'
            return JsonResponse(json_error)

        statistic = get_cities_statistic(question_id, country_name)
        self.statistic = statistic

        return super().get(request, *args, **kwargs)

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)
        json['data'] = self.statistic
        return json


class GeneralQuestionInfoView(JsonView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        question_id = kwargs.get('quest_id')
        quest = Question.objects.filter(id=question_id).first()

        if not quest:
            json_error = self.get_error_data()
            json_error['error'] = 404
            json_error['message'] = 'Вопрос не найден'
            return JsonResponse(json_error)

        ans_count = Answer.objects.filter(choice__question_id=question_id).count()
        users_count = User.objects.filter(is_active=True).count()

        self.info = {
            'author': quest.author.get_full_name(),
            'date': datetime.strftime(quest.pub_date, '%d.%m.%Y'),
            'title': quest.title,
            'rating': quest.rating,
            'answers_count': ans_count,
            'users_count': users_count
        }

        return super().get(request, *args, **kwargs)

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)
        json['data'] = self.info
        return json


class CountryCityView(JsonView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        country_name = request.GET.get('country')

        country = None
        is_exists = False
        if country_name:
            country_name = unquote(country_name, 'utf-8')
            country = get_country(country_name)
            is_exists = User.objects.filter(country=country).exists()

        if not is_exists:
            json_error = self.get_error_data()
            json_error['error'] = 404
            json_error['message'] = 'Страна не найдена'
            return JsonResponse(json_error)

        cities = User.objects.filter(country=country).values('city__international_name').annotate(
                count=Count('city__international_name'))

        self.cities_list = []
        for city in cities:
            self.cities_list.append(city['city__international_name'])

        return super().get(request, *args, **kwargs)

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)
        data = []
        for city in self.cities_list:
            data.append({'title': city})
        json['data'] = data
        return json


class GetCitySuggests(JsonView):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        first_chars = self.request.GET.get('text')
        max_count = 3
        self.cities_list = []

        if first_chars:
            countries = City.objects.filter(
                    Q(international_name__startswith=first_chars) | Q(original_name__startswith=first_chars) | Q(
                        cyrillic_name__startswith=first_chars))[:max_count]

            for country in countries:
                self.cities_list.append(country.cyrillic_name)

        return super().get(request, *args, **kwargs)

    def get_json_data(self, **kwargs):
        json = super().get_json_data(**kwargs)
        suggestions = []
        for city in self.cities_list:
            suggestions.append(city)
        json['data']['suggestions'] = suggestions
        return json
