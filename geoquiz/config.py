from configparser import RawConfigParser


class SafeConfigParser:
    def __init__(self, **kwargs):
        self.raw_config = RawConfigParser(**kwargs)

    def get(self, section, option, default=None):
        try:
            return self.raw_config.get(section, option)
        except Exception:
            return default

    def getint(self, section, option, default=None):
        try:
            return self.raw_config.getint(section, option)
        except Exception:
            return default

    def getboolean(self, section, option, default=None):
        try:
            return self.raw_config.getboolean(section, option)
        except Exception:
            return default

    def getfloat(self, section, option, default=None):
        try:
            return self.raw_config.getfloat(section, option)
        except Exception:
            return default

    def read(self, filenames, encoding=None):
        return self.raw_config.read(filenames, encoding)
