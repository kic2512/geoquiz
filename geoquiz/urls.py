from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from geoquiz import settings

urlpatterns = [
    url(r'^', include('core.urls', namespace='core')),
    url(r'^', include('quiz.urls', namespace='quiz')),
    url(r'^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
