/**
 * Created by max on 16.01.16.
 */

define([
    'backbone'
], function (Backbone) {

    var Manager = {

        subscribe: function (views) {
            for (var I in views) {
                this.listenTo(views[I], 'rerender', this.rerender);
                this.listenTo(views[I], 'reshow', this.reshow);
            }
        },

        unsubscribe: function (view) {
            this.stopListening(view);
        },

        rerender: function (view) {
            view.render();
            setMDLHandlers();
            this.reshow(view);
        },

        triggerView: function (eventName) {
            this.trigger(eventName, this.currentView);
        },

        hideCurrent: function () {
            if (this.currentView) {
                this.triggerView('view-hide');

                var r = $.Deferred();
                this.currentView.$el.hide(function () {
                    r.resolve();
                });
                return r;
            }
            return $.when();
        },

        reshow: function (view) {
            this.hideCurrent().done(function () {
                this.currentView = view;
                this.triggerView('view-show');
                view.$el.show();
            }.bind(this));
        },

        isCurrent: function (view) {
            return this.currentView === view;
        }

    };

    return _.extend(Manager, Backbone.Events);
});