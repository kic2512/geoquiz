/**
 * Created by max on 07.02.16.
 */

define([
    'backbone',
    'underscore'
], function (Backbone, _) {

    var View = Backbone.View.extend({
        el: $('#legend'),
        queTitle: '',
        shareText: '',
        template: _.template($('#legend-tml').html()),

        events: {
            'click .js-next-question': 'nextQuestion',
            'click .js-to-statistics': 'toDiagrams',
            'click .js-share': 'openShareChoice',
            'click .hide-legend-button': 'toggleLegend'
        },

        initialize: function () {
            return this;
        },

        nextQuestion: function (event) {
            this.trigger('next-question');
        },

        toDiagrams: function (event) {
            this.trigger('to-statistics');
        },

        countTotalVoices: function (countryData) {
            var count = 0;
            for (var I in countryData) {
                if (countryData.hasOwnProperty(I)) {
                    count += countryData[I][1];
                }
            }
            return count;
        },

        addPercentages: function (countryData) {
            var countVoices = this.countTotalVoices(countryData);
            for (var I in countryData) {
                if (countryData.hasOwnProperty(I)) {
                    countryData[I].push(((countryData[I][1] / countVoices) * 100).toFixed(1) + "%");
                }
            }
        },

        render: function (data, location) {
            var withColors = true;
            if (!data) {
                var countryData = window.worldStat;
                data = [];
                for (var I in countryData) {
                    if (countryData.hasOwnProperty(I)) {
                        data.push([
                            countryData[I].title,
                            countryData[I].count
                        ]);
                    }
                }
                location = 'По всем старанам';
            }
            this.addPercentages(data);
            var shareData = {
                url: window.location.toString(),
                title: this.queTitle,
                image: window.shareImage,
                description: this.shareText
            };
            this.$el.html(this.template({
                data: data,
                title: this.queTitle,
                location: location,
                colors: window.MARKER_COLORS,
                labelColors: window.LABEL_COLORS,
                withColors: withColors
            }));
            this.setShare(shareData);
            return this;
        },

        setShare: function (data) {
            Ya.share2('#my-share', {
                content: data,
                contentByService: {
                    facebook: data,
                    vkontakte: data,
                    odnoklassniki: data,
                    moimir: data
                },
                theme: {
                    services: 'vkontakte,facebook,odnoklassniki,moimir'
                }
            });
        },

        show: function (queTitle, shareText, doneCb) {
            this.queTitle = queTitle;
            this.shareText = shareText;

            this.render();
            this.$el.show(function () {
                if (doneCb) doneCb();
            });
            return this;
        },

        hide: function (doneCb) {
            this.$el.hide(function () {
                if (doneCb) doneCb();
            });
            return this;
        },

        up: true,

        toggleLegend: function (event) {
            event.preventDefault();
            var button = this.$('.hide-legend-button');

            this.$('#legend-container').slideToggle(function () {
                if (this.up) {
                    button.html('<i class="material-icons">keyboard_arrow_up</i>');
                    this.up = false;
                }
                else {
                    button.html('<i class="material-icons">keyboard_arrow_down</i>');
                    this.up = true;
                }
            }.bind(this));
        },

        openShareChoice: function () {
            this.$('.js-share-choice').slideToggle('slow');
        }
    });

    return new View();
});