/**
 * Created by kic on 1/19/16.
 */

define([
    'backbone',
    'underscore',
    'models/question'
], function (Backbone, _, Question) {

    var View = Backbone.View.extend({
        el: $('#question-details'),
        template: _.template($('#question-tml').html()),
        commentTemplate: _.template($('#comment-tml').html()),

        initialize: function () {
            this.addBtn = $('#add-btn');

            this.hide();
            return this;
        },

        events: {
            'click .js-question-variant': 'chooseVariant',
            'click .js-to-sign-in': 'goToSignIn',
            'click .js-to-sign-up': 'goToSignUp',
            'click .js-change-rating': 'changeRating',
            'click .js-add-comment': 'addComment',
            'click .js-to-results': 'toResults'
        },

        render: function () {
            this.model.fetch({
                success: function (model) {
                    var question = model.toJSON();

                    var hasAnswer = false;
                    for (var I = 0; I < question.choices.length; I++)
                        if (question.choices[I].has_voted) {
                            hasAnswer = true;
                            break;
                        }

                    this.$el.html(this.template({
                        que: question,
                        renderComment: this.commentTemplate,
                        hasAnswer: hasAnswer
                    }));
                }.bind(this),

                error: function (model, response) {
                    console.log('error');
                    console.log(response);
                }.bind(this)
            });

            return this;
        },

        show: function (queId) {
            this.model.url = window.ServerUrls['questionDetail'] + queId + '/';
            this.addBtn.show(800, function () {
                this.trigger('rerender', this);
            }.bind(this));
            return this;
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        chooseVariant: function (event) {
            event.preventDefault();
            var variantOption = $(event.currentTarget);

            var choiceId = variantOption.data('id');
            var choiceText = $.trim(variantOption.html());
            var queId = variantOption.parent().data('id');

            var isAnonym = !window.isAuthenticated;
            if (isAnonym) {
                if (localStorage.length == 4) {
                    this.showWarning('Ваши ответы не учитываются, т.к. вы не авторизованы.' +
                    'После авторизации все ваши ответы засчитаются.');
                }
                localStorage.setItem(queId, choiceId);
                this.trigger('to-statistic', queId, choiceText);
            }
            else {
                this.model.chooseVariant(choiceId)
                    .done(function () {
                        this.trigger('to-statistic', queId, choiceText);
                    }.bind(this))
                    .fail(function (message) {
                        this.showWarning(message);
                    }.bind(this));
            }
        },

        fetchPollError: function (message) {
            this.showSnackbar(message);
        },

        showWarning: function (message) {
            this.showSnackbar(message);
        },

        goToSignIn: function () {
            event.preventDefault();
            this.trigger('to-sign-in');
        },

        goToSignUp: function () {
            event.preventDefault();
            this.trigger('to-sign-up');
        },

        changeRating: function (event) {
            if (window.isAuthenticated !== true) {
                this.showWarning('Чтобы проголосовать за вопрос войдите или зарегистрируйтесь');
                return;
            }

            var btnElement = $(event.currentTarget);
            var block = btnElement.parent();
            var ratingContainer = block.find('.rating-container');

            var url = this.model.attributes.vote_url;

            var data = null;

            var model = this.model;
            var view = this;

            btnElement.prop('disabled', true);
            $.post(url, data, function (response) {
                if ('status' in response && response.status === 'OK') {
                    var item;
                    if (response.data.has_increase === true) {
                        model.attributes.has_voted = true;
                        item = btnElement.find('i');
                        item.html('favorite');
                        btnElement.addClass('mdl-button--colored');
                    }
                    else {
                        item = btnElement.find('i');
                        item.html('favorite_border');
                        btnElement.removeClass('mdl-button--colored');
                        model.attributes.has_voted = false;
                    }
                    ratingContainer.html(response.data.rating);
                }

                else if ('error' in response) {
                    view.showWarning(response.error);
                }

            }).
            error (function (response) {
                    view.showWarning('Сервер не доступен попробуйте позже');
            }).
            always(function() {
                btnElement.prop('disabled', false);
            });
        },

        toResults: function (event) {
            var que_id = $(event.currentTarget).attr('data-id');
            this.trigger('to-global', que_id,'');
        },

        addComment: function (event) {

            if (window.isAuthenticated !== true) {
                this.showWarning('Чтобы оставить комментарий войдите или зарегистрируйтесь');
            }

            var container = $(event.target);
            var commentBtn = container.parent();

            var comment = $('#comment');
            var text = comment.val();

            if (text == '') {
                this.showWarning('Введите текст');
                return;
            }

            var view = this;
            var model = this.model;

            var url = this.model.attributes.comment_url;
            var data = {
                text: text
            };

            commentBtn.prop('disabled', true);
            $.post(url, data, function (response) {

                if ('status' in response && response.status === 'OK') {
                    var new_comment = response.data.comment;
                    var el = view.commentTemplate({
                        comment: new_comment
                    });

                    comment.val('');
                    $('#comment-container').prepend($(el));

                }

                else if ('error' in response) {
                    view.showWarning(response.error);
                }

            }).error(function (response) {

                view.showWarning('Сервер не доступен попробуйте позже');

            }).always(function(){
                commentBtn.prop('disabled', false);
            });
        }

    });

    var question = new Question();
    return new View({model: question});

});