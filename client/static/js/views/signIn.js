/**
 * Created by max on 16.01.16.
 */

define([
    'backbone',
    'underscore',
    'models/user'
], function (Backbone, _, UserModel) {

    var View = Backbone.View.extend({
        el: $('#sign-in'),
        template: _.template($('#sign-in-tml').html()),

        events: {
            'click .js-clear': 'clearInputs',
            'click .js-sign-in': 'signIn',
            'submit form': 'submit'
        },

        initialize: function () {
            this.navbar = $('.js-user-navbar');
            this.addBtn = $('#add-btn');

            this.render();
            this.hide();
            return this;
        },

        render: function () {
            this.$el.html(this.template());
            return this;
        },

        show: function () {
            this.addBtn.hide(800, function () {
                this.trigger('reshow', this);
            }.bind(this));
            return this;
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        clearInputs: function () {
            //this.$('.mdl-textfield__input').val('');
            this.trigger('to-poll');
        },

        submit: function (event) {
            event.preventDefault();
            this.signIn();
        },

        signIn: function () {
            var emailInput = this.$('#login_id');
            var email = emailInput.val();
            var passwordInput = this.$('#password_id');
            var password = passwordInput.val();

            this.model.signIn(email, password)
                .done(function (redirectUrl, message) {
                    window.isAuthenticated = true;
                    this.trigger('set-user', this.model);
                    this.showSuccess(message);

                    this.setNavbar(false);
                    this.setAddButton(false);

                    window.setMDLHandlers();
                    window.setClientNavigation();

                    this.trigger('redirect', redirectUrl);
                }.bind(this))
                .fail(function (message, wrongInput) {
                    if (wrongInput) {
                        if (wrongInput === 'email')
                            emailInput.parent().addClass('is-invalid');
                        else if (wrongInput === 'password')
                            passwordInput.parent().addClass('is-invalid');
                    }
                    this.showWarning(message);
                }.bind(this));
        },

        setNavbar: function (anonymFlag) {
            if (this.navbar) {
                var navbarTml = (!anonymFlag)?
                    $('#user-navbar-tml').html() :
                    $('#anonym-navbar-tml').html();
                var userNavbar = _.template(navbarTml);

                this.navbar.html(userNavbar({
                    user: this.model
                }));
            }
        },

        setAddButton: function (anonymFlag) {
            if (this.addBtn) {
                var content = '';
                if (!anonymFlag) {
                    var addBtnTml = $('#add-button-tml').html();
                    content = _.template(addBtnTml);
                }
                this.addBtn.html(content);
            }
        },

        logout: function () {
            if (window.isAuthenticated) {
                this.model.logout()
                    .done(function (redirectUrl, message) {
                        window.isAuthenticated = false;

                        this.showSuccess(message);
                        this.setNavbar(true);
                        this.setAddButton(true);

                        window.setMDLHandlers();
                        window.setClientNavigation();

                        this.trigger('redirect', redirectUrl);
                    }.bind(this))
                    .fail(function (message) {
                        this.showWarning(message);
                    }.bind(this));
            }
        },

        identifyMe: function () {
            this.model.identify()
                .done(function (message) {
                    window.isAuthenticated = true;
                    this.trigger('set-user', this.model);
                }.bind(this))
                .fail(function (message) {
                    this.showWarning(message);
                }.bind(this));
        },

        showSuccess: function (message) {
            this.showSnackbar(message);
        },

        showWarning: function (message) {
            this.showSnackbar(message);
        }
    });
    var userModel = new UserModel();
    return new View({model: userModel});
});