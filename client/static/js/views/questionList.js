/**
 * Created by max on 18.01.16.
 */

define([
    'backbone',
    'underscore',
    'collections/questions',
    'managers/viewMan'
], function (Backbone, _, queCollection, viewMan) {

    var View = Backbone.View.extend({
        el: $('#questions'),
        template: _.template($('#questions-tml').html()),
        itemTemplate: _.template($('#questions-item-tml').html()),
        sortOrder: 0,

        initialize: function () {
            this.addBtn = $('#add-btn');

            this.hide();
            return this;
        },

        events: {
            'click .js-to-que-details': 'chooseQuestion',
            'click .js-like': 'changeRating',
            'click .js-to-create': 'toCreate'
        },

        render: function () {
            this.collection.fetch({
                data: {
                    sort: this.sortOrder,
                    extra: this.extra
                },
                success: function () {
                    var queList = this.collection.toJSON();
                    this.$el.html(this.template({
                        questions: queList,
                        renderQuestion: this.itemTemplate
                    }));
                }.bind(this),

                error: function () {
                    this.fetchPollError('Ошибка обращения к серверу');
                }.bind(this)
            });
            return this;
        },

        show: function (sortOrder, extraParam) {
            var activeTab = $('#js-tabs>.is-active');
            var sortTab = $('#' + sortOrder + '-tab');

            this.sortOrder = this.collection.sort[sortOrder];

            if (extraParam) {
                this.extra = this.collection.extra[extraParam];
            }
            else {
                this.extra = 0;
            }
            this.addBtn.show(800, function () {
                if (activeTab)
                    activeTab.removeClass('is-active');
                if (sortTab)
                    sortTab.addClass('is-active');
                this.trigger('rerender', this);
            }.bind(this));
            return this;
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        chooseQuestion: function (e) {
            var id = $(e.currentTarget).attr('data-id');
            this.trigger('to-question', id);
        },

        changeRating: function (event) {
            if (window.isAuthenticated !== true) {
                this.showWarning('Чтобы проголосовать за вопрос войдите или зарегистрируйтесь');
                return;
            }

            var btnElement = $(event.currentTarget);
            var block = btnElement.parent();
            var ratingContainer = block.find('.question-item__rating');

            var url = btnElement.attr('data-url');

            if (url === undefined) {
                console.log('not url');
                return;
            }

            var data = null;
            var view = this;
            btnElement.prop('disabled', true);
            $.post(url, data, function (response) {
                if ('status' in response && response.status === 'OK') {
                    var item;
                    if (response.data.has_increase === true) {

                        btnElement.addClass('mdl-button--colored');
                        item = btnElement.find('i');
                        item.html('favorite');
                    }
                    else {

                        item = btnElement.find('i');
                        item.html('favorite_border');
                        btnElement.removeClass('mdl-button--colored');
                    }
                    ratingContainer.html(response.data.rating);
                }

                else if ('error' in response) {
                    view.showWarning(response.error);
                }

            }).
            error (function (response) {
                    view.showWarning('Сервер не доступен. Попробуйте позже');
            }).
            always(function() {
                btnElement.prop('disabled', false);
            });
        },

        toCreate: function () {
            this.trigger('to-create');
        },

        showWarning: function (message) {
            this.showSnackbar(message);
        }

    });
    return new View({collection: queCollection});
});