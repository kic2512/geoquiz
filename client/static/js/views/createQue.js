/**
 * Created by max on 05.02.16.
 */

define([
    'backbone',
    'underscore',
    'models/question',
    'managers/viewMan',
    'moment'
], function (Backbone, _, Question, viewMan, moment) {

    var View = Backbone.View.extend({
        el: $('#create-question'),
        template: _.template($('#create-question-tml').html()),

        events: {
            'click .js-clear': 'clearInputs',
            'click .js-create': 'create',
            'click .js-add-choice': 'addChoice',
            'submit form': 'submit'
        },

        initialize: function () {
            this.addBtn = $('#add-btn');
            this.choiceCnt = 2;
            this.listenTo(viewMan, 'view-hide', this.onHide);
            this.render();
            this.hide();
            return this;
        },

        render: function () {
            this.$el.html(this.template());
            return this;
        },

        show: function () {
            if (window.isAuthenticated) {
                this.addBtn.hide(800, function () {
                    this.trigger('reshow', this);
                }.bind(this));
            }
            return this;
        },

        onHide: function () {
            if (viewMan.isCurrent(this)) {
                this.addBtn.show(800);
            }
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        clearInputs: function () {
            //this.$('.mdl-textfield__input').val('');
            this.trigger('to-poll');
        },

        addChoice: function () {
            var choiceTml = $('#add-choice-tml').html();
            var choice = _.template(choiceTml)({
                choiceNum: ++this.choiceCnt
            });
            this.$('.js-choices-stack').append(choice);
            window.setMDLHandlers();
        },

        submit: function (event) {
            event.preventDefault();
            this.create();
        },

        create: function () {
            var choices = [];
            this.$('.js-choice').each(function () {
                var choiceVal = $(this).val();
                if (choiceVal)
                    choices.push(choiceVal);
            });

            var titleInput = this.$('#title_id');
            var title = titleInput.val();
            if (title == '') {
                titleInput.parent().addClass('is-invalid');
                this.showWarning('Поле заголовка не может быть пустым');
                return;
            }

            this.model.set({
                title: title,
                pub_date: moment().format(),
                choices: choices
            });

            this.model.save(null, {
                success: function (model, response) {
                    if ('status' in response && response.status == 'OK') {
                        this.render();
                        this.showSuccess('Опрос успешно создан');
                        this.trigger('to-my-polls');
                    }
                    else if ('error' in response) {
                        this.showWarning(response.error);
                    }
                }.bind(this),

                error: function (model, response) {
                    this.showWarning('Ошибка сети, проверьте свое подклчение');
                }.bind(this)
            });
        },

        showSuccess: function (message) {
            this.showSnackbar(message);
        },

        showWarning: function (message) {
            this.showSnackbar(message);
        }
    });

    var question = new Question();
    return new View({model: question});
});