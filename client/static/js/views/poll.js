/**
 * Created by max on 16.01.16.
 */

define([
    'backbone',
    'underscore',
    'models/poll'
], function (Backbone, _, Poll) {

    var View = Backbone.View.extend({
        el: $('#poll'),
        template: _.template($('#poll-tml').html()),

        events: {
            'click .js-poll-option': 'chooseOption',
            'click .js-poll-vote': 'changeRating',
            'click .js-to-create': 'toCreate'
        },

        initialize: function () {
            this.addBtn = $('#add-btn');
            this.listenTo(this.model, 'fetch-error', this.fetchPollError);

            this.hide();
            return this;
        },

        render: function () {
            this.model.fetch({
                success: function (model) {
                    this.$el.html(this.template({
                        que: model.toJSON()
                    }));
                }.bind(this),

                error: function () {
                    this.fetchPollError('Ошибка обращения к серверу');
                }.bind(this)
            });
            return this;
        },

        show: function () {
            var activeTab = $('#js-tabs>.is-active');
            var pollTab = $('#poll-tab');

            this.addBtn.show(800, function () {
                this.trigger('rerender', this);
                if (activeTab)
                    activeTab.removeClass('is-active');
                if (pollTab)
                    pollTab.addClass('is-active');
            }.bind(this));
            return this;
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        chooseOption: function (event) {
            event.preventDefault();
            var pollOption = $(event.currentTarget);

            var choiceId = pollOption.data('id');
            var choiceText = $.trim(pollOption.html());
            var queId = pollOption.parent().data('id');

            var isAnonym = !window.isAuthenticated;
            if (isAnonym) {
                if (localStorage.length == 4) {
                    this.showWarning('Ваши ответы не учитываются, т.к. вы не авторизованы.' +
                    'После авторизации все ваши ответы засчитаются.');
                }
                localStorage.setItem(queId, choiceId);
                this.trigger('to-statistic', queId, choiceText);
            }
            else {
                this.model.chooseOption(choiceId)
                    .done(function () {
                        this.trigger('to-statistic', queId, choiceText);
                    }.bind(this))
                    .fail(function (message) {
                        this.showWarning(message);
                    }.bind(this));
            }
        },

        changeRating: function (event) {
            if (window.isAuthenticated !== true) {
                this.showWarning('Чтобы проголосовать за вопрос войдите или зарегистрируйтесь');
                return;
            }

            var btnElement = $(event.currentTarget);
            var block = btnElement.parent();
            var ratingContainer = block.find('.poll__rating-container');

            var url = this.model.attributes.vote_url;

            var data = null;

            var model = this.model;
            var view = this;

            btnElement.prop('disabled', true);
            $.post(url, data, function (response) {
                if ('status' in response && response.status === 'OK') {
                    var item;
                    if (response.data.has_increase === true) {
                        model.attributes.has_voted = true;
                        btnElement.addClass('mdl-button--colored');
                        item = btnElement.find('i');
                        item.html('favorite');
                    }
                    else {
                        btnElement.removeClass('mdl-button--colored');
                        item = btnElement.find('i');
                        item.html('favorite');
                        model.attributes.has_voted = false;
                    }
                    ratingContainer.html(response.data.rating);
                }

                else if ('error' in response) {
                    view.showWarning(response.error);
                }

            }).
            error (function (response) {
                    view.showWarning('Сервер не доступен попробуйте позже');
            }).
            always(function() {
                btnElement.prop('disabled', false);
            });
        },

        fetchPollError: function (message) {
            this.showSnackbar(message);
        },

        showWarning: function (message) {
            this.showSnackbar(message);
        },

        toCreate: function () {
            this.trigger('to-create');
        }
    });
    var poll = new Poll();
    return new View({model: poll});
});