/**
 * Created by max on 17.01.16.
 */

define([
    'backbone',
    'underscore',
    'managers/viewMan',
    'views/legend'
], function (Backbone, _, viewMan, legend) {

    var View = Backbone.View.extend({
        el: $('.lower-block'),
        choiceText: '',
        currentView: null,
        currentQueId: null,

        events: {
            'click .js-next-question': 'nextQuestion',
            'click .js-to-statistics': 'toDiagrams'
        },

        initialize: function () {
            this.centralPopup = $('.questions_popup');
            this.listenTo(viewMan, 'view-hide', this.onHide);
            this.listenTo(legend, 'next-question', this.nextQuestion);
            this.listenTo(legend, 'to-statistics', this.toDiagrams);
        },

        render: function () {
            return this;
        },

        show: function (queId) {
            this.currentQueId = queId;
            this.centralPopup.hide(800, function () {
                this.trigger('reshow', this);

                window.queryStatistic(queId, this.choiceText)
                    .done(function () {
                        legend.show(queTitle, shareText);
                    });
            }.bind(this));
            return this;
        },

        onHide: function () {
            if (viewMan.isCurrent(this)) {
                legend.hide(function () {
                    this.centralPopup.show(800);
                }.bind(this));
            }
        },

        nextQuestion: function () {
            legend.hide(function () {
                this.centralPopup.show(800, function () {
                    this.trigger('to-poll');
                }.bind(this));
            }.bind(this));
        },

        toDiagrams: function () {
            this.centralPopup.show(800, function () {
                this.trigger('to-diagrams', this.currentQueId);
            }.bind(this));
        },

        markerClick: function (data, location) {
            legend.render(data, location);
        }
    });

    return new View();
});