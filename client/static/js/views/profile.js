/**
 * Created by max on 25.01.16.
 */

define([
    'backbone',
    'underscore',
    'models/user',
    'datepicker',
    'mdlselectfield'
], function (Backbone, _, UserModel, Datepicker) {

    var View = Backbone.View.extend({
        el: $('#profile'),
        template: _.template($('#profile-tml').html()),

        events: {
            'click .js-clear': 'clearInputs',
            'click .js-save-user': 'saveUser',
            'submit form': 'submit',
            'keyup .js-city-suggests': 'getSuggestsCities',
            'click .js-close': 'toPoll'
        },

        initialize: function () {
            this.addBtn = $('#add-btn');

            this.hide();
            return this;
        },

        render: function () {
            if (this.model) {
                this.$el.html(this.template({
                    user: this.model.toJSON(),
                    fullName: this.model.getFullName()
                }));
            }
            $('#birthday_id').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
            return this;
        },

        show: function () {
            if (window.isAuthenticated && this.model) {
                this.addBtn.hide(800, function () {
                    this.trigger('reshow', this);
                }.bind(this));
            }
            else {
                this.trigger('to-sign-in');
            }
            return this;
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        clearInputs: function () {
            this.$('.mdl-textfield__input').val('');
        },

        submit: function (event) {
            event.preventDefault();
            this.saveUser();
        },

        saveUser: function () {

            this.model.set({
                email: $('#profile_email_id').val(),
                first_name: $('#profile_first_name_id').val(),
                last_name: $('#profile_last_name_id').val(),
                country: $('#country_id').val(),
                city: $('#city_id').val(),
                gender: $('.profile_gender_container input:checked').val(),
                date_of_birth: $('#birthday_id').val()
            });

            this.model.save(null, {
                success: function (model, response) {
                    if ('status' in response && response.status == 'OK') {
                        this.render();
                        this.showSuccess('Ваши данные успешно сохранены');
                        this.trigger('to-poll');
                    }
                    else if ('error' in response) {
                        this.showWarning(response.error);
                    }
                }.bind(this),

                error: function (model, response) {
                    this.showWarning('Ошибка сети, проверьте свое подключение');
                }.bind(this)
            });
        },

        getSuggestsCities: function (e) {
            var text = $(e.target).val();
            var url = window.ServerUrls.countrySuggestions;

            $.get(url, {text: text}, function (response) {
                if ('data' in response && 'suggestions' in response.data) {
                    $( "#city_id" ).autocomplete({
                        source: response.data.suggestions
                    });
                }
            }).fail(function (xhr, textStatus) {
                console.log(textStatus);
            });

        },

        toPoll: function () {
            this.trigger('to-poll');
        },

        showWarning: function (message) {
            this.showSnackbar(message);
        },

        showSuccess: function (message) {
            this.showSnackbar(message);
        }

    });
    return new View({model: UserModel});
});