/**
 * Created by max on 16.01.16.
 */

define([
    'backbone',
    'underscore',
    'models/user'
], function (Backbone, _, UserModel) {

    var View = Backbone.View.extend({
        el: $('#sign-up'),
        template: _.template($('#sign-up-tml').html()),

        events: {
            'click .js-clear': 'clearInputs',
            'click .js-reg-in': 'register',
            'submit form': 'submit'
        },

        initialize: function () {
            this.addBtn = $('#add-btn');

            this.render();
            this.hide();
            return this;
        },

        render: function () {
            this.$el.html(this.template());
            return this;
        },

        show: function () {
            this.addBtn.hide(800, function () {
                this.trigger('reshow', this);
            }.bind(this));
            return this;
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        clearInputs: function () {
            //this.$('.mdl-textfield__input').val('');
            this.trigger('to-poll');
        },

        submit: function (event) {
            event.preventDefault();
            this.register();
        },

        register: function () {
            var emailInput = this.$('#login_id');
            var email = emailInput.val();

            var passwordInput = this.$('#password_id');
            var password = passwordInput.val();

            var repeatedInput = this.$('#repeat_password_id');
            var repeated = repeatedInput.val();

            if (password !== repeated) {
                repeatedInput.parent().addClass('is-invalid');
                repeatedInput.val('');
                return;
            }

            this.model.register(email, password, repeated)
                .done(function (redirectUrl, message) {
                    this.showSuccess(message);
                    this.trigger('redirect', redirectUrl);
                }.bind(this))
                .fail(function (message, wrongInput) {
                    if (wrongInput) {
                        if (wrongInput === 'email')
                            emailInput.parent().addClass('is-invalid');
                        else if (wrongInput === 'password')
                            passwordInput.parent().addClass('is-invalid');
                        else if (wrongInput === 'repeated')
                            repeatedInput.parent().addClass('is-invalid');
                    }
                    this.showWarning(message);
                }.bind(this));
        },

        showSuccess: function (message) {
            this.showSnackbar(message);
        },

        showWarning: function (message) {
            this.showSnackbar(message);
        }
    });

    var userModel = new UserModel();
    return new View({model: userModel});
});