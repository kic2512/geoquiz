define([
    'backbone',
    'underscore',
    'diagrams/diagram-utils',
    'diagrams/age-diagram',
    'diagrams/date-diagram',
    'diagrams/gender-diagram',
    'managers/viewMan',
    'collections/countries',
    'collections/cities',
    'models/question'
], function (Backbone, _, diagramUtils, ageDiagram, dateDiagram,
             genderDiagram, viewMan, countriesCollection, citiesCollection,
             Question) {

    var View = Backbone.View.extend({
        el: $('#diagrams'),
        template: _.template($('#diagrams-tml').html()),

        events: {
            "change #countries-selector": "countrySelected",
            "change #cities-selector": "citiesSelected",
            'click .js-to-map': 'returnToMap'
        },

        updateDiagrams: function (params) {
            genderDiagram.updateDiagram(this.queId, params);
            ageDiagram.updateDiagram(this.queId, params);
            dateDiagram.updateDiagram(this.queId, params);
        },

        citiesSelected: function () {
            var cityTitle = $('#cities-selector').val();
            var params;
            var countryTitle = $('#countries-selector').val();
            params = {'country': decodeURIComponent(countryTitle)};
            if (cityTitle != '') {
                 params['city'] = decodeURIComponent(cityTitle);
            }
            this.updateDiagrams(params);
        },

        countrySelected: function () {
            var countryTitle = $('#countries-selector').val();
            var params = {'country': decodeURIComponent(countryTitle)};
            this.updateDiagrams(params);
            var combobox = $('#cities-combobox');
            if (countryTitle == '') {
                combobox.hide();
            } else {
                combobox.show();
            }
            var citiesSelector = $('#cities-selector');
            citiesSelector.find('option:gt(0)').remove(); // remove all options, but not the first
            var select = citiesSelector[0];
            countryTitle = decodeURIComponent(countryTitle);
            this.collection.cities.setCountryTitle(countryTitle);
            this.collection.cities.fetch({
                success: function () {
                    this.collection.cities.forEach(function (item, i, arr) {
                        var cityTitle = arr[i].attributes.title;
                        select.options[i + 1] = new Option(cityTitle, cityTitle);
                    });
                }.bind(this),

                error: function () {
                    this.fetchPollError('Ошибка обращения к серверу');
                }.bind(this)
            });

        },

        initialize: function () {
            this.addBtn = $('#add-btn');
            this.backMap = $('.background-map');
            this.hide();
            this.listenTo(viewMan, 'view-hide', this.onHide);
            return this;
        },

        render: function () {
            this.model.url = window.ServerUrls['questionDetail'] + this.queId + '/';
            this.model.fetch({
                success: function (model) {
                    this.renderDiagrams(model.toJSON());
                }.bind(this),
                error: function (model, response) {
                    console.log('error');
                    console.log(response);
                }.bind(this)
            });
            this.collection.countries.setQuestionId(this.queId);

            return this;
        },

        renderDiagrams: function(question) {
            this.collection.countries.fetch({
                success: function () {
                    this.$el.html(this.template({
                        countries: this.collection.countries.toJSON(),
                        question: question
                    }));
                    genderDiagram.renderDiagram(this.queId);
                    ageDiagram.renderDiagram(this.queId);
                    dateDiagram.renderDiagram(this.queId);
                }.bind(this),

                error: function () {
                    this.fetchPollError('Ошибка обращения к серверу');
                }.bind(this)
            });
        },

        show: function (queId) {
            this.queId = queId;
            $('body').css({
                'height': '100%',
                'margin': '0',
                'background-repeat': 'no-repeat',
                'background-attachment': 'fixed',
                'background': '#ECE9E6',
                'background': '-webkit-linear-gradient(to left, #ECE9E6 , #FFFFFF)',
                'background': 'linear-gradient(to left, #ECE9E6 , #FFFFFF)'
            });
            this.backMap.hide(function () {
                this.$el.show(function () {
                    this.addBtn.hide(800, function () {
                        this.trigger('rerender', this);
                    }.bind(this));
                }.bind(this));
            }.bind(this));
            return this;
        },

        onHide: function (view) {
            if (viewMan.isCurrent(this)) {
                $('body').css('background', '#575757');
                this.backMap.show();
            }
        },

        hide: function () {
            this.$el.hide();
            return this;
        },

        returnToMap: function () {
            $('body').css('background', '#575757');
            this.backMap.show(function () {
                this.trigger('to-map', this.queId);
            }.bind(this));
        }
    });

    var question = new Question();
    return new View({
        collection: {
            countries: countriesCollection,
            cities: citiesCollection
        },
        model: question
    });
});