/**
 * Created by max on 16.01.16.
 */

require.config({
    //urlArgs: "bust=" + (new Date()).getTime(),
    baseUrl: "/static/js",
    paths: {
        'set-csrf': "set-csrf",
        'jquery': "libs/jquery-1.11.3.min",
        'jquery.cookie': "libs/jquery.cookie",
        'moment': "libs/moment",
        'underscore': "libs/underscore-min",
        'backbone': "libs/backbone",
        'bootstrap': "libs/bootstrap.min",
        'datepicker': "libs/bootstrap-material-datetimepicker",
        'tagsinput': "libs/bootstrap-tagsinput.min",
        'material': "libs/material",
        'google.jsapi': "//www.google.com/jsapi?dummy=",
        'google.maps': "//maps.googleapis.com/maps/api/js?v=3&key=AIzaSyDlA_CCrKpAyGq9P3NNVNMQxWSrIVe1awk&sensor=false&callback=initMap&dummy=",
        'initialize-maps': "maps/initialize-map",
        'marker-with-label': "maps/marker-with-label",
        'statistic': "maps/statistic",
        'highstock': "libs/highstock",
        'exporting': "libs/exporting",
        'mdlselectfield': "libs/mdl-selectfield.min",
        'suggestions': "libs/suggestions"
    },
    shim: {
        'bootstrap': {deps:['jquery']},
        'tagsinput': {deps:['jquery', 'bootstrap']},
        'material': {deps:['jquery']},
        'mdlselectfield': {deps: ['jquery', 'material']},
        'google.maps': {deps:['google.jsapi', 'initialize-maps']},
        'initialize-maps': {deps:['google.jsapi']},
        'marker-with-label': {deps:['google.jsapi', 'google.maps']},
        'statistic': {deps:['jquery', 'initialize-maps']},
        'exporting': {deps:['highstock']},
        'suggestions': {deps: ['jquery']}
    }
});

define([
    'set-csrf',
    'backbone',
    'router',
    'jquery',
    'material',
    'datepicker',
    'tagsinput',
    'marker-with-label',
    'statistic',
    'exporting',
    'mdlselectfield',
    'suggestions'
], function(
    csrf,
    Backbone,
    router,
    $
){
    function setClientNavigation () {
        var selectorToURI = {
            '.js-to-sign-in': 'sign-in',
            '.js-to-sign-up': 'sign-up',
            '.js-to-poll': '',
            '.js-to-hot': 'questions/hot',
            '.js-to-best': 'questions/best',
            '.js-to-new': 'questions/new',
            '.js-to-profile': 'profile',
            '.js-to-logout': 'logout',
            '.js-to-create-quiz': 'create-quiz',
            '.js-to-my-quiz': 'my-polls',
            '.js-to-pass-quiz': 'answered-polls'
        };

        for (var selector in selectorToURI) {
            if (selectorToURI.hasOwnProperty(selector)) {

                $(selector).click(function (event) {
                    event.preventDefault();
                    router.navigate(this.uri, {trigger: true});
                }.bind({
                    uri: selectorToURI[selector]
                }));
            }
        }
    }
    window.setClientNavigation = setClientNavigation;

    function setMDLHandlers () {
        componentHandler.upgradeDom();
    }
    window.setMDLHandlers = setMDLHandlers;

    window.MARKER_COLORS = ['colour-1', 'colour-2', 'colour-3', 'colour-4', 'colour-5', 'colour-6',
                 'colour-7', 'colour-8', 'colour-9'];

    window.LABEL_COLORS = ['colour-1b', 'colour-2b', 'colour-3b', 'colour-4b', 'colour-5b', 'colour-6b',
                 'colour-7b', 'colour-8b', 'colour-9b'];

    Backbone.View.prototype.showSnackbar = function (message) {
        var toast = $('#toast')[0];
        if (toast) {
            toast.MaterialSnackbar.showSnackbar({
                message: message,
                timeout: 2750
            });
        }
    };

    $(document).ready(function () {
        setClientNavigation();
        setMDLHandlers();

        $(document).ajaxStop(function () {
            setMDLHandlers();
        });
    });

    Backbone.history.start();

    if (window.location.hash == "#_=_")
        window.location.hash = "";

    if (window.force_navigate) {
        router.navigate(window.force_navigate, {trigger: true});
    }
    else if (window.location.hash){
        router.navigate(window.location.hash, {trigger: true});
    }
    else {
        router.navigate('', {trigger: true});
    }
});