/**
 * Created by max on 16.01.16.
 */

define([
    'backbone',
    'views/signIn',
    'views/signUp',
    'views/profile',
    'views/poll',
    'managers/viewMan',
    'views/global',
    'views/questionList',
    'views/question',
    'views/diagrams',
    'views/createQue'
], function (Backbone,
             signIn, signUp, profile,
             poll, viewMan, global, questionList, question,
             diagrams, createQue)
{
    window.globalView = global;

    profile.listenTo(signIn, 'set-user', function (userModel) {
        this.model = userModel;
        this.render();
    });

    if (window.isAuthenticated) {
        signIn.identifyMe();
    }

    viewMan.subscribe([signIn, signUp, profile, poll, global, questionList, question, diagrams, createQue]);

    var Router = Backbone.Router.extend({

        initialize: function () {
            this.listenTo(poll, 'to-statistic', this.toGlobal);
            this.listenTo(question, 'to-statistic', this.toGlobal);
            this.listenTo(question, 'to-sign-in', this.toSignIn);
            this.listenTo(question, 'to-sign-up', this.toSignUp);
            this.listenTo(question, 'to-global', this.toGlobal);
            this.listenTo(global, 'to-diagrams', this.toDiagrams);
            this.listenTo(global, 'to-poll', this.toPoll);
            this.listenTo(questionList, 'to-question', this.toQuestion);
            this.listenTo(signIn, 'redirect', this.redirectTo);
            this.listenTo(signIn, 'to-profile', this.toProfile);
            this.listenTo(signIn, 'to-poll', this.toPoll);
            this.listenTo(signUp, 'redirect', this.redirectTo);
            this.listenTo(signUp, 'to-poll', this.toPoll);
            this.listenTo(profile, 'to-sign-in', this.toSignIn);
            this.listenTo(profile, 'to-poll', this.toPoll);
            this.listenTo(createQue, 'to-poll', this.toPoll);
            this.listenTo(createQue, 'to-my-polls', this.toMyPolls);
            this.listenTo(poll, 'to-create', this.toCreate);
            this.listenTo(questionList, 'to-create', this.toCreate);
            this.listenTo(diagrams, 'to-map', this.toGlobal);
        },

        routes: {
            '': "poll",
            'sign-in': "signIn",
            'sign-up': "signUp",
            'profile': "profile",
            'logout': "logout",

            'global-map/:queId': "global",
            'questions/:sort': 'questionList',
            'question/:queId': "question",
            'create-quiz': "createQuiz",
            'my-polls': 'myPolls',
            'answered-polls': 'answeredPolls',
            'diagrams/:queId': 'diagrams'
        },

        redirectTo: function (url) {
            this.navigate(url, {trigger: true});
        },

        poll: function () {
            poll.show();
        },

        toPoll: function () {
            this.navigate('', {trigger: true});
        },

        signIn: function () {
            signIn.show();
        },

        toSignIn: function () {
            this.navigate('sign-in', {trigger: true});
        },

        toSignUp: function () {
            this.navigate('sign-up', {trigger: true});
        },

        signUp: function () {
            signUp.show();
        },

        profile: function () {
            profile.show();
        },

        toProfile: function (userModel) {
            profile.model = userModel;
            this.navigate('profile', {trigger: true});
        },

        logout: function () {
            signIn.logout();
        },

        global: function (queId) {
            global.show(queId);
        },

        toGlobal: function (queId, choiceText) {
            global.choiceText = choiceText || '';

            var uri = 'global-map/' + queId;
            this.navigate(uri, {trigger: true});
        },

        diagrams: function (queId) {
            diagrams.show(queId);
        },

        toDiagrams: function (queId) {
            var uri = 'diagrams/' + queId;
            this.navigate(uri, {trigger: true});
        },

        questionList: function (sort) {
            questionList.show(sort);
        },

        myPolls: function () {
            questionList.show('new', 'my');
        },

        toMyPolls: function () {
            this.navigate('my-polls', {trigger: true});
        },

        answeredPolls: function () {
            questionList.show('new', 'answered');
        },

        toQuestion: function (queId) {
            this.navigate('question/' + queId, {trigger: true});
        },

        question: function (queId) {
            question.show(queId);
        },

        createQuiz: function () {
            createQue.show();
        },

        toCreate: function () {
            this.navigate('create-quiz', {trigger: true});
        }
    });
    return new Router();
});