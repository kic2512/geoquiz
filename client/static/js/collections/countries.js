/**
 * Created by max on 31.01.16.
 */
define([
    'backbone',
    'models/country'
], function (Backbone,
             Country) {

    var Collection = Backbone.Collection.extend({
        model: Country,

        setQuestionId: function (queId) {
            this.queId = queId;
        },

        url: function () {
            return window.ServerUrls['countries'] + this.queId;
        },

        parse: function (resp) {
            return resp.data;
        }
    });

    return new Collection();
});