/**
 * Created by max on 31.01.16.
 */
define([
    'backbone',
    'models/city'
], function(
    Backbone,
    Cities
){

    var Collection = Backbone.Collection.extend({
        model: Cities,

        setCountryTitle: function (countryTitle) {
            this.countryTitle = countryTitle;
        },

        url: function () {
            return window.ServerUrls['cities'] + '?country=' + this.countryTitle;
        },

        parse: function (resp) {
            return resp.data;
        }
    });

    return new Collection();
});