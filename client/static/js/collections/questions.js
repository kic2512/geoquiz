/**
 * Created by max on 18.01.16.
 */

define([
    'backbone',
    'models/question'
], function(
    Backbone,
    Question
){

    var Collection = Backbone.Collection.extend({
        model: Question,
        url: window.ServerUrls['questionList'],

        page: 0,
        pagesCnt: 0,
        sort: {
            'new': 1,
            'best': 2,
            'hot': 3
        },

        extra: {
            'my': 1,
            'answered': 2
        },

        parse: function (response, options) {
            if ('data' in response) {
                this.page = response.data['page'];
                this.pagesCnt = response.data['pagesCnt'];

                var collectionData = response.data['questions'];
                options.parse = false;
                return Backbone.Collection.prototype.parse.call(this, collectionData, options);
            }
            else if ('error' in response) {
                this.trigger('fetch-error', response.error);
                return {};
            }
        }
    });

    return new Collection();
});