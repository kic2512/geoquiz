/**
 * Created by max on 18.01.16.
 */


define([
    'backbone'
], function(
    Backbone
){

    var Model = Backbone.Model.extend({
        url: window.ServerUrls['questionDetail'],

        defaults: {
            author_email: '',
            author_full_name: '',
            title: '',
            text: '',
            pub_date: '',
            rating: 0,
            comments_cnt: '',
            choices: [],
            comments: [],
            has_voted: false,
            has_answer: false,
            vote_url: "",
            comment_url: "",
            topic: 0
        },

        initialize: function() {
        },

        sync: function(method, model, options) {
            options = options || {};

            if (method == 'create') {
                options.url = window.ServerUrls['createQuestion'];
                options.type = 'POST';
            }
            return Backbone.sync(method, model, options);
        },

        parse: function (response, options) {
            if ('data' in response) {
                if ('que' in response.data)
                    return Backbone.Model.prototype.parse.call(this, response.data.que, options);


            }
            else if ('error' in response) {
                this.trigger('fetch-error', response.error);
                return {};
            }
        },

        chooseVariant: function (choiceId) {
            var r = $.Deferred();
            var url = window.ServerUrls.answerQuestion;

            $.post(url, {choice: choiceId})
                .done(function (response) {
                    if (response.status === 'OK') {
                        r.resolve();
                    }
                    else {
                        r.reject(response.error);
                    }
                })
                .fail(function (jqXHR, textStatus) {
                    r.reject('Сервер недоступен');
                });
            return r;
        }
    });

    return Model;
});