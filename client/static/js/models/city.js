/**
 * Created by max on 31.01.16.
 */
define([
    'backbone'
], function(
    Backbone
){

    var Model = Backbone.Model.extend({
        defaults: {
            title: ''
        }

    });

    return Model;
});