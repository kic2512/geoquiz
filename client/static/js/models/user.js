/**
 * Created by max on 19.01.16.
 */

define([
    'backbone'
], function(
    Backbone
) {

    var Model = Backbone.Model.extend({
        url: window.ServerUrls['profile'],

        netFailMsg: 'Ошибка соединения',
        serverFailMsg: 'Ошибка сервера',
        invalidDataMsg: 'Некорректные данные',

        defaults: {
            email: '',
            first_name: '',
            last_name: '',
            country: '',
            city: '',
            gender: '',
            username: '',
            date_of_birth: '',
            avatar: ''

        },

        sync: function(method, model, options) {
            options = options || {};

            if (method == 'create') {
                options.url = window.ServerUrls['editProfile'];
            }
            return Backbone.sync(method, model, options);
        },

        getFullName: function () {
            if (this.first_name || this.last_name)
                return this.first_name + ' ' + this.last_name;
            else
                return this.email;
        },

        loginUrl: window.ServerUrls['login'],
        loginRedirect: 'profile',

        signIn: function (email, password) {
            var r = $.Deferred();
            $.post(this.loginUrl, {email: email, password: password})
                .done(function (response) {

                    if ('status' in response && response.status === 'OK') {
                        var message = response.data['message'];
                        var user = response.data['user'];

                        if (this.set(user, {}))
                            r.resolve(this.loginRedirect, message);
                        else
                            r.reject('signIn: ' + this.invalidDataMsg);
                    }
                    else {
                        if ('error' in response)
                            r.reject(response.error, 'email');
                        else
                            r.reject('signIn: ' + this.serverFailMsg);
                    }
                }.bind(this))
                .fail(function (xhr, textStatus) {
                    r.reject('signIn: ' + this.netFailMsg);
                }.bind(this));
            return r;
        },

        registerUrl: window.ServerUrls['register'],
        registerRedirect: 'sign-in',

        register: function (email, password, repeated) {
            var r = $.Deferred();
            $.post(this.registerUrl, {email: email, password: password})
                .done(function (response) {

                    if ('status' in response && response.status === 'OK') {
                        var message = response.data['message'];
                        r.resolve(this.registerRedirect, message);
                    }
                    else {
                        if ('error' in response)
                            r.reject(response.error, 'email');
                        else
                            r.reject('signUp: ' + this.serverFailMsg);
                    }
                }.bind(this))
                .fail(function (xhr, textStatus) {
                    r.reject('signUp: ' + this.netFailMsg);
                }.bind(this));
            return r;
        },

        logoutUrl: window.ServerUrls['logout'],
        logoutRedirect: '',

        logout: function () {
            var r = $.Deferred();
            $.get(this.logoutUrl)
                .done(function (response) {

                    if ('status' in response && response.status === 'OK') {
                        var message = response.data['message'];
                        r.resolve(this.logoutRedirect, message);
                    }
                    else {
                        if ('error' in response)
                            r.reject(response.error);
                        else
                            r.reject('logout: ' + this.serverFailMsg);
                    }
                }.bind(this))
                .fail(function (xhr, textStatus) {
                    r.reject('logout: ' + this.netFailMsg);
                }.bind(this));
            return r;
        },

        identifyUrl: window.ServerUrls['identify'],

        identify: function () {
            var r = $.Deferred();
            $.get(this.identifyUrl, {})
                .done(function (response) {
                    if ('status' in response && response.status === 'OK') {
                        var message = response.data['message'];

                        if (this.set(response.data, {}))
                            r.resolve(message);
                        else
                            r.reject('identify: ' + this.invalidDataMsg);
                    }
                    else {
                        if ('error' in response)
                            r.reject(response.error);
                        else
                            r.reject('identify: ' + this.serverFailMsg);
                    }
                }.bind(this))
                .fail(function (xhr, textStatus) {
                    r.reject('identify: ' + this.netFailMsg);
                }.bind(this));
            return r;
        }
    });

    return Model;
});