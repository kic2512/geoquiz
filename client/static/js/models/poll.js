/**
 * Created by max on 17.01.16.
 */

define([
    'backbone'
], function(
    Backbone
){

    var Model = Backbone.Model.extend({
        url: window.ServerUrls['nextWonder'],

        defaults: {
            author_email: '',
            author_full_name: '',
            title: '',
            text: '',
            pub_date: '',
            rating: 0,
            comments_cnt: '',
            choices: []
        },

        initialize: function() {},

        parse: function (response, options) {
            if ('data' in response) {
                return Backbone.Model.prototype.parse.call(this, response.data, options);
            }
            else if ('error' in response) {
                this.trigger('fetch-error', response.error);
                return {};
            }
        },

        chooseOption: function (choiceId) {
            var r = $.Deferred();
            var url = window.ServerUrls.answerQuestion;

            $.post(url, {choice: choiceId})
                .done(function (response) {
                    if (response.status === 'OK') {
                        r.resolve();
                    }
                    else {
                        r.reject(response.error);
                    }
                })
                .fail(function (jqXHR, textStatus) {
                    r.reject('Сервер недоступен');
                });
            return r;
        }
    });

    return Model;
});
