from django.http import JsonResponse
from django.middleware.csrf import get_token, rotate_token
from django.views.generic import View, DetailView, CreateView, ListView, UpdateView, DeleteView


class JsonResponseMixin:
    def get_json_data(self, **kwargs):
        request = self.request
        json = {'status': 'OK', 'data': {}}
        if request.method in ['POST', 'PUT']:
            json['csrf_token'] = get_token(request)
        return json

    def get_error_data(self, **kwargs):
        return {'status': 'ERROR', 'error': None}

    def objects_to_dict(self, objects):
        return [obj.as_dict() for index, obj in enumerate(objects)]

    def json_error(self, message, *args, **kwargs):
        json = self.get_error_data(**kwargs)
        json['error'] = message
        return JsonResponse(json)


class JsonView(JsonResponseMixin, View):
    def get(self, request, *args, **kwargs):
        json = self.get_json_data(**kwargs)
        return JsonResponse(json)

    def post(self, request, *args, **kwargs):
        json = self.get_json_data(**kwargs)
        return JsonResponse(json)


class JsonDetailView(JsonResponseMixin, DetailView):
    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)
        json = self.get_json_data(**kwargs)
        return JsonResponse(json)


class JsonCreateView(JsonResponseMixin, CreateView):
    def form_valid(self, form):
        self.object = form.save()
        json = self.get_json_data()
        json['data'].update({
            'url': self.get_success_url()
        })
        return JsonResponse(json)

    def form_invalid(self, form):
        json = self.get_error_data()
        return JsonResponse(json)

    def get_success_url(self):
        return None


class JsonListView(JsonResponseMixin, ListView):
    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)
        json = self.get_json_data(**kwargs)
        return JsonResponse(json)


class JsonUpdateView(JsonResponseMixin, UpdateView):
    def form_valid(self, form):
        self.object = form.save()
        json = self.get_json_data()
        json['data'].update({
            'url': self.get_success_url()
        })
        return JsonResponse(json)

    def form_invalid(self, form):
        json = self.get_error_data()
        return JsonResponse(json)

    def get_success_url(self):
        return None


class JsonDeleteView(JsonResponseMixin, DeleteView):
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()

        json = self.get_json_data()
        json['data'].update({
            'url': self.get_success_url()
        })
        return JsonResponse(json)

    def get_success_url(self):
        return None
