from django.http import HttpResponseBadRequest, HttpResponseForbidden
from django.utils.decorators import method_decorator

__author__ = 'max'


def login_required(view_function):
    def wrap(request, *args, **kwargs):
        if request.user.is_anonymous():
            return HttpResponseForbidden()
        return view_function(request, *args, **kwargs)

    wrap.__doc__ = view_function.__doc__
    wrap.__name__ = view_function.__name__
    return wrap


def ajax_required(view_function):
    def wrap(request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        return view_function(request, *args, **kwargs)

    wrap.__doc__ = view_function.__doc__
    wrap.__name__ = view_function.__name__
    return wrap


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)
